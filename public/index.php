<?php

use Framework\Core\App;

require '../vendor/autoload.php';

App::init();
$response = App::run();
if ($response) {
    App::handleResponse($response);
}
