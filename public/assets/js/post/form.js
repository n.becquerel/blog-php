$(document).ready(function () {
    tinyMCE.init({
        selector: '#post_content',
        language: 'fr_FR',
        plugins: "autoresize link code",
        // menubar: "insert",
        // toolbar: " link",
        toolbar: 'undo redo | styleselect | fontselect fontsizeselect | bold italic underline code | alignleft aligncenter alignright' +
            ' alignjustify | backcolor forecolor | link',
        default_link_target: "_blank",
        statusbar: false
    });
});