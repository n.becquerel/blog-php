$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    // hide alert messages
    setTimeout(function () {
        $('.js-flash-message').fadeOut(500, function () {
            $(this).remove();
        });
    }, 5000);
});