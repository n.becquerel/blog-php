$(document).ready(function () {
    $(document).on('mouseenter', '.contact-i', function () {
        $(this).addClass('fa-envelope-open').removeClass('fa-envelope');
    }).on('mouseleave', '.contact-i', function () {
        $(this).addClass('fa-envelope').removeClass('fa-envelope-open');
    });
});