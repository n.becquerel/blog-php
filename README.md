<h4>
    Last codacy analysis
</h4>
<a href="https://www.codacy.com/manual/becquerel.nicolas/blog-php?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=n.becquerel/blog-php&amp;utm_campaign=Badge_Grade"><img src="https://app.codacy.com/project/badge/Grade/3200c9500a374974bf6ee5796bc16fe9"/></a>
<a href="https://app.codacy.com/manual/becquerel.nicolas/blog-php/dashboard?bid=19857249">Link to last analysis</a>
<h4>
    Production server
</h4>
<a href="http://blog-php.nico-dev.com/" target="_blank">Demo here</a>
<h1>Quick start</h1>
<h2>Prerequisite</h2>
<p>
    First of all you need to make sure your environment matches few prerequisite.
</p>
<ul>
    <li>PHP 7.2 or higher</li>
    <li>ext-pdo must be installed and enabled</li>
    <li>ext-yaml must be installed and enabled</li>    
</ul>
<p>
    You will need git and composer too to install the project.
</p>
<h2>Clone project</h2>
<p>
    Once you meet the requirements you can clone the project :
</p>

```bash
git clone https://gitlab.com/n.becquerel/blog-php.git blog-php
```

<p>
    Once project is cloned go inside the directory and install libraries with composer :
</p>

```bash
cd blog-php
composer install
```

<h2>Create database</h2>
<p>
    You can then setup database importing the build/database.sql into your database manager.
    To add some entries to test project, you can import build/data.sql into your database manager.
</p>
<p>
    Then the important thing to make project work with your database is to verify the config in config/parameters.yaml 
    file under the key 'database:'
</p>

<h2>Done</h2>
<p>
    Now the project installed and ready to work you just have to run the server and make sure the root is set to public/ 
    via a virtual host.
</p>
<p>
    With php cli: 
</p>

```bash
# with php cli
php -S localhost:8000 -t public 
```

<p>If you used provided data sheme, you can now log in and start adding some content in existing posts or create new ones with :</p>
<ul>
    <li>admin@blog-php.com, password</li>
    <li>user@blog-php.com, password</li>
</ul>
