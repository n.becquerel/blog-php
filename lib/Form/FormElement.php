<?php

namespace Framework\Form;

/**
 * Class FormElement
 * Used to represent a form element containing label and input elements
 *
 * @package Framework\Form
 */
class FormElement
{
    /**
     * @var string
     */
    private $fieldName;

    /**
     * @var InputElement
     */
    private $input;

    /**
     * @var LabelElement
     */
    private $label;

    /**
     * @var ErrorElement
     */
    private $error;

    /**
     * FormElement constructor.
     * @param string $fieldName
     * @param InputElement $input
     * @param LabelElement $label
     * @param ErrorElement $error
     */
    public function __construct(string $fieldName, InputElement $input, LabelElement $label, ErrorElement $error)
    {
        $this->fieldName = $fieldName;
        $this->input = $input;
        $this->label = $label;
        $this->error = $error;
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * @return InputElement
     */
    public function getInput(): InputElement
    {
        return $this->input;
    }

    /**
     * @return LabelElement
     */
    public function getLabel(): LabelElement
    {
        return $this->label;
    }

    /**
     * @return ErrorElement
     */
    public function getError(): ErrorElement
    {
        return $this->error;
    }
}
