<?php

namespace Framework\Form;

class ErrorElement
{
    /**
     * @var string
     */
    private $fieldName;

    /**
     * @var array
     */
    private $messages;

    /**
     * ErrorElement constructor.
     * @param string $fieldName
     * @param array $messages
     */
    public function __construct(string $fieldName, array $messages = [])
    {
        $this->fieldName = $fieldName;
        $this->messages = $messages;
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}
