<?php

namespace Framework\Form;

use http\Env\Response;

/**
 * Class InputElement
 *
 * Used to represent an input element (html)
 *
 * @package Framework\Form
 */
class InputElement implements HtmlElement
{
    /**
     * The name Attr
     *
     * @var string
     */
    private $name;

    /**
     * The id attr
     *
     * @var string
     */
    private $id;

    /**
     * Others attr
     *
     * @var array
     */
    private $attrs = [];

    /**
     * The html tag used to represent the input
     *
     * @var string
     */
    private $type;

    /**
     * @var mixed|null
     */
    private $value;
    /**
     * @var array|null
     */
    private $choices;

    /**
     * Globals options (defined in the form class)
     * @var array
     */
    private $globals;

    /**
     * InputElement constructor.
     * @param string $name
     * @param string $id
     * @param string $type
     * @param array|null $attrs
     * @param array|null $choices
     * @param mixed|null $value
     * @param array $globals
     */
    public function __construct(
        string $name,
        string $id,
        string $type,
        array $attrs = null,
        array $choices = null,
        $value = null,
        $globals = []
    ) {
        $this->name = $name;
        $this->id = $id;
        $this->attrs = $attrs;
        $this->type = $type;
        $this->value = $value;
        $this->choices = $choices;
        $this->globals = $globals;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getAttrs(): array
    {
        return $this->attrs;
    }

    /**
     * Add an attr without overriding if already exist
     * Accept only a key as attr name, and a value
     * @param array $attr
     * @return $this
     */
    public function addAttr(array $attr): self
    {
        $attrs = $this->getAttrs();

        if (isset($attrs[key($attr)])) {
            $existingAttr = $attrs[key($attr)];
            $existingAttr .= ' ' . $attr[key($attr)];
            $attrs[key($attr)] = $existingAttr;
        } else {
            $attr[] = $attr;
        }

        $this->attrs = $attrs;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return array
     */
    public function getChoices(): array
    {
        return $this->choices;
    }

    /**
     * @return array
     */
    public function getGlobals(): array
    {
        return $this->globals;
    }
}
