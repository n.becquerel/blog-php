<?php

namespace Framework\Form;

/**
 * Class LabelElement
 *
 * Used to represent a label element (html)
 *
 * @package Framework\Form
 */
class LabelElement implements HtmlElement
{
    /**
     * For attr
     *
     * @var string
     */
    private $for;

    /**
     * @var array
     */
    private $attrs = [];

    /**
     * @var string
     */
    private $value;

    /**
     * LabelElement constructor.
     * @param string $for
     * @param string $value
     * @param array $attrs
     */
    public function __construct(string $for, string $value, array $attrs = [])
    {
        $this->for = $for;
        $this->attrs = $attrs;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getFor(): string
    {
        return $this->for;
    }

    /**
     * @return array
     */
    public function getAttrs(): array
    {
        return $this->attrs;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
