<?php

namespace Framework\Form;

use Framework\Orm\EntityInterface;

interface FormInterface
{
    /**
     * FormInterface constructor.
     */
    public function __construct();

    /**
     * Used to register form fields and options for each fields.
     * Validation constraints are used here
     *
     * @param array $options
     *
     * @return void
     */
    public function buildForm(array $options = []): void;

    /**
     * Used to prefix all fields of the form
     *
     * @return string
     */
    public function getPrefix(): string;

    /**
     * Used to get the className of the entity bounded to the form
     *
     * @return string
     */
    public function getClassName(): string;
}
