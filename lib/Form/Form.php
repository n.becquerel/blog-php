<?php

namespace Framework\Form;

use Assert\Assert;
use Assert\InvalidArgumentException;
use Assert\LazyAssertionException;
use Framework\Container\Container;
use Framework\Orm\EntityInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class Form
 * Used to represent a form
 *
 * @package Framework\Form
 */
abstract class Form implements FormInterface
{
    /**
     * Entity Bound to the form
     *
     * @var EntityInterface|null
     */
    private $entity = null;

    /**
     * Array of form fields
     *
     * @var array
     */
    private $fields = [];

    /**
     * @var string
     */
    private $prefix;

    /**
     * @var string
     */
    private $classname;

    /**
     * Contains all the error messages of the form
     *
     * @var array
     */
    private $errors;

    /**
     * Form constructor.
     */
    public function __construct()
    {
        $this->prefix = $this->getPrefix();
        $this->classname = $this->getClassname();
    }

    /**
     * @param object|null $entity
     */
    public function init($entity = null)
    {
        if (!$entity) {
            return;
        }
        $this->entity = $entity;
    }

    /**
     * Used to register a field in the form
     *
     * @param string $fieldName
     * @param string $type
     * @param array $options
     *
     * @return Form
     */
    protected function add(string $fieldName, string $type, array $options = []): self
    {
        $this->fields[$fieldName] = [
            'type' => $type,
            'options' => $options
        ];

        return $this;
    }

    /**
     * Create a collection of FormElements
     *
     * @param int|null $index
     * @param bool $isPrototype
     * @return array
     */
    public function createView(int $index = null, bool $isPrototype = false): array
    {
        $fields = [];

        foreach ($this->fields as $name => $options) {
            if ($options['type'] === 'prototype') {
                $i = 0;
                $formStr = $options['options']['class'];

                if ($this->getValue($name)) {
                    foreach ($this->getValue($name) as $value) {
//                    dump($value);
                        $form = Container::getInstance()->get($formStr);
                        $form->init($value);
                        $form->buildForm();
//                    dump($form);
                        $fields[$name]['elements'][] = $form->createView($i);
                        $i++;
                    }
                }

                $form = Container::getInstance()->get($formStr);
                $class = $form->getClassName();
                $form->init(new $class());
                $form->buildForm();
                $fields[$name]['prototype'] = $form->createView(null, true);
//                die;
            } else {
                $formElement = $this->createElement($name, $options, $index, $isPrototype);
                $fields[$formElement->getFieldName()] = $formElement;
            }
        }

        return $fields;
    }

    /**
     * Search for value in entity and return it escaped
     *
     * @param string $fieldName
     *
     * @return mixed
     */
    private function getValue(string $fieldName)
    {
        $getter = 'get' . ucfirst($fieldName);

        if ($value = $this->entity->$getter()) {
            return $value;
        }

        return null;
    }

    /**
     * Hydrate the stored entity searching in request body with the prefix of the form
     *
     * @param ServerRequestInterface $serverRequest
     */
    public function handleRequest(ServerRequestInterface $serverRequest): void
    {
        $post = $serverRequest->getParsedBody();
        if (isset($this->entity) && isset($post[$this->prefix])) {
            $post = $post[$this->prefix];
            $reflector = new \ReflectionClass($this->getClassName());

            foreach ($post as $name => $value) {
                if (is_array($value)) {
                    array_walk_recursive($value, function ($value) {
                        $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
                    });
                    $setter = 'add' . rtrim(preg_replace("/$this->prefix/", '', $name), 's');
                    $getter = 'get'  . preg_replace("/$this->prefix/", '', $name);
                } else {
                    if (!isset($this->fields[$name]['options']['auto_escape']) ||
                        (
                            isset($this->fields[$name]['options']['auto_escape']) &&
                            $this->fields[$name]['options']['auto_escape'] === true
                        )
                    ) {
                        $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
                    }

                    $setter = 'set' . preg_replace("/$this->prefix/", '', $name);
                    $getter = 'get'  . preg_replace("/$this->prefix/", '', $name);
                }

                if ($reflector->hasMethod($setter)) {
                    $meth = $reflector->getMethod($setter);
                    $params = $meth->getParameters();
                    if ($params &&
                        is_int(strpos($params[0]->getType()->getName(), 'App\Entity'))
                    ) {
                        if ($this->fields[$name]['type'] === 'prototype') {
                            $subEntityStr = $params[0]->getType()->getName();

                            $i = 0;
                            foreach ($value as $v) {
                                if (isset($this->entity->$getter()[$i])) {
                                    $subEntity = $this->entity->$getter()[$i];
                                } else {
                                    $subEntity = new $subEntityStr();
                                }

                                foreach ($v as $prop => $val) {
                                    $subSetter = 'set' . $prop;
                                    $subEntity->$subSetter($val);
                                }
                                $this->entity->$setter($subEntity);
                                $i++;
                            }
                        } elseif ($this->fields[$name]['type'] === 'entity') {
                            $repository = Container::getInstance()->get($this->fields[$name]['options']['repository']);
                            $value = $repository->find($value);
                            $this->entity->$setter($value);
                        }
                    } else {
                        $this->entity->$setter($value);
                    }
                } else {
                    $this->entity->$setter($value);
                }
            }
        }
    }

    /**
     * Use the config used in the build form to validate entity
     * If constraint fails, set errors in $this
     *
     * @return bool
     */
    public function isValid(): bool
    {
        $lazyAssert = Assert::lazy()->tryAll();

        // get constraints foreach fields
        foreach ($this->fields as $fieldName => $options) {
            $constraints = isset($options['options']['constraints']) ? $options['options']['constraints'] : [];

            foreach ($constraints as $method => $args) {
                $lazyAssert->that($this->getValue($fieldName), $this->prefix . '_' . $fieldName);
                call_user_func_array([$lazyAssert, $method], $args);
            }
        }

        try {
            $lazyAssert->verifyNow();
        } catch (LazyAssertionException $e) {
            $this->setErrors($e->getErrorExceptions());
        }

        return empty($this->errors);
    }

    /**
     * @param string $fieldName
     * @param array|null $options
     * @param int|null $index
     * @return FormElement
     */
    private function createElement(
        string $fieldName,
        array $options = null,
        int $index = null,
        bool $isPrototype = false
    ): FormElement {
        if (is_int($index)) {
            $id = $this->prefix . '_' . (string) $index . '_' . $fieldName;
        } elseif ($isPrototype) {
            $id = $this->prefix . '_' . '__index__' . '_' . $fieldName;
        } else {
            $id = $this->prefix . '_' . $fieldName;
        }

        $value = null;

        if ((isset($options['options']['mapped']) && $options['options']['mapped'] === true) ||
            !isset($options['options']['mapped'])
        ) {
            $value = $this->getValue($fieldName);
        }

        if (isset($options['options']['mapped']) && $options['options']['mapped'] === false) {
            $name = $fieldName;
        } elseif (is_int($index)) {
            $name = "$this->prefix[$index][$fieldName]";
        } elseif ($isPrototype) {
            $name = "$this->prefix[__index__][$fieldName]";
        } else {
            $name = "$this->prefix[$fieldName]";
        }

        $choices = [];
        if ($options['type'] === 'entity') {
            if (isset($options['options']['choices'])) {
                $choices = isset($options['options']['choices']);
            } else {
                $choices = Container::getInstance()->get($options['options']['repository'])->findAll();
            }
        }

        $inputElement = new InputElement(
            $name,
            $id,
            $options['type'],
            isset($options['options']['attr']) ? $options['options']['attr'] : [],
            $choices,
            $value,
            $options
        );

        $labelElement = new LabelElement(
            $id,
            isset($options['options']['label']) ? $options['options']['label'] : $fieldName,
            isset($options['options']['label_attr']) ? $options['options']['label_attr'] : []
        );

        $errors = isset($this->errors[$inputElement->getId()]) ? $this->errors[$inputElement->getId()] : [];
        $errorElement = new ErrorElement($fieldName, $errors);

        return new FormElement($fieldName, $inputElement, $labelElement, $errorElement);
    }

    /**
     * Normalize errors and set them in this
     *
     * @param array $errors
     */
    private function setErrors(array $errors): void
    {
        /** @var InvalidArgumentException $error */
        foreach ($errors as $error) {
            $this->errors[$error->getPropertyPath()][] = $error->getMessage();
        }
    }
}
