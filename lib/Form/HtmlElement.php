<?php

namespace Framework\Form;

interface HtmlElement
{
    /**
     * Get the html attrs of html element
     *
     * @return array
     */
    public function getAttrs(): array;
}
