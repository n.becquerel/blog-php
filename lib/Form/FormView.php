<?php

namespace Framework\Form;

class FormView
{
    /**
     * Array of elements representing the field
     * collection of FormElement
     * @var FormElement[]
     */
    private $fields = [];

    /**
     * @return FormElement[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param FormElement $element
     */
    public function addField(FormElement $element)
    {
        $this->fields[$element->getFieldName()] = $element;
    }

    /**
     * @param string $fieldName
     * @return FormElement
     * @throws NotFoundException
     */
    public function getField(string $fieldName)
    {
        if (isset($this->fields[$fieldName])) {
            return $this->fields[$fieldName];
        }

        throw new NotFoundException(
            sprintf(
                'Field : "%s" unknow in formView',
                $fieldName
            )
        );
    }
}
