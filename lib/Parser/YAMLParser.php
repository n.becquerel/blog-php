<?php

namespace Framework\Parser;

use InvalidArgumentException;
use function yaml_parse_file;

class YAMLParser
{
    /**
     * Path to root folder
     * @var string
     */
    private $path = null;

    /**
     * @var YAMLParser
     */
    public static $instance;

    /**
     * YAMLParser constructor.
     */
    public function __construct()
    {
        $this->path = __DIR__ . '/../..';
    }

    /**
     * Read a yaml file and return its content (YAML file MUST be in /src/config folder)
     * If file does not exist, return empty array
     * @param string $fileName
     * @return array|null
     */
    public function getContent($fileName): ?array
    {
        if ($this->hasFile($fileName)) {
            return yaml_parse_file($this->path . '/'. $fileName . '.yaml');
        }

        return [];
    }

    /**
     * @param $fileName
     * @throws InvalidArgumentException
     * @return bool
     */
    private function hasFile($fileName)
    {
        if (!is_string($fileName)) {
            throw new InvalidArgumentException(
                'yaml file path must be string, ' . gettype($fileName) . ' given'
            );
        }

        if (file_exists($this->path . '/' . $fileName . '.yaml')) {
            return true;
        }

        return false;
    }

    /**
     * @return YAMLParser
     */
    public static function getInstance(): YAMLParser
    {
        if (!self::$instance) {
            self::$instance = new YAMLParser();
        }

        return self::$instance;
    }

    /**
     * Return new instance with new path
     * @param string $path
     * @return YAMLParser
     */
    public function withPath(string $path)
    {
        if (!is_string($path)) {
            throw new InvalidArgumentException('Path must be string');
        }

        $clone = clone $this;
        $clone->path = $this->path . '/' . $path;
        return $clone;
    }
}
