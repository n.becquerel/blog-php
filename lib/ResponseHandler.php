<?php

namespace Framework;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Handle ResponseInterface
 * Class ResponseHandler
 * @package Framework
 */
class ResponseHandler
{
    public function handleResponse(ResponseInterface $response)
    {
        $this->writeHeader($response);
        $this->writeBody($response->getBody());
    }

    private function writeHeader(ResponseInterface $response)
    {
        foreach ($response->getHeaders() as $name => $values) {
            foreach ($values as $value) {
                header(sprintf('%s: %s', $name, $value), false);
            }
        }

        $mainHead = 'HTTP/' . $response->getProtocolVersion() . ' ' . $response->getStatusCode() . ' ' .
            $response->getReasonPhrase();
        
        header($mainHead);
    }

    private function writeBody(?StreamInterface $body)
    {
        echo $body;
    }
}
