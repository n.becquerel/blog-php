<?php

namespace Framework\Security\Encoder;

use Framework\Security\UserInterface;

interface UserPasswordEncoderInterface
{
    /**
     * Returns a UserInterface with password set to hashed password
     * @param UserInterface $user
     * @param string $plainPassword
     * @return UserInterface
     */
    public function encodeUserPassword(UserInterface $user, string $plainPassword): UserInterface;
}
