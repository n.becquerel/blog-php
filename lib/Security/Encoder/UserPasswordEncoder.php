<?php

namespace Framework\Security\Encoder;

use Framework\Security\UserInterface;

class UserPasswordEncoder implements UserPasswordEncoderInterface
{
    public function encodeUserPassword(UserInterface $user, string $plainPassword): UserInterface
    {
        return $user->setPassword($this->bCryptEncode($plainPassword));
    }

    private function bCryptEncode(string $plainText): string
    {
        return password_hash($plainText, PASSWORD_BCRYPT);
    }
}
