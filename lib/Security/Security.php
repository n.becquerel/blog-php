<?php

namespace Framework\Security;

use Framework\Security\Providers\UserProviderInterface;

class Security
{
    /**
     * @var UserProviderInterface
     */
    private $userProvider;

    /**
     * Security constructor.
     * @param UserProviderInterface $userProvider
     */
    public function __construct(UserProviderInterface $userProvider)
    {
        $this->userProvider = $userProvider;
    }

    public function getUser(): ?UserInterface
    {
        return $this->userProvider->getUser();
    }

    public function isGranted(string $role): bool
    {
        $user = $this->userProvider->getUser();

        if (!$user) {
            return false;
        }

        return in_array($role, $user->getRoles());
    }

    public function isAuthenticated(): bool
    {
        $user = $this->userProvider->getUser();

        if (!$user) {
            return false;
        }

        return true;
    }
}
