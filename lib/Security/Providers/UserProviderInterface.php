<?php

namespace Framework\Security\Providers;

use Framework\Security\UserInterface;

interface UserProviderInterface
{
    /**
     * Return the current user authenticated
     * @return UserInterface
     */
    public function getUser(): ?UserInterface;
}
