<?php

namespace Framework\Security;

use Framework\Orm\EntityInterface;

interface UserInterface extends EntityInterface, \Serializable
{
    public function setPassword(string $password);

    public function getPassword(): ?string;

    public function setRoles(array $roles);

    public function getRoles(): array;

    /**
     * Remove data you don't want to be exposed in the app (like password - even hashed password)
     */
    public function eraseCredentials(): void;
}
