<?php

namespace Framework\Security;

interface AuthenticatorInterface
{
    public function getCredentials(array $bodyRequest): ?array;

    public function getUser(array $credentials): ?UserInterface;

    public function checkCredentials(array $credentials, UserInterface $user): bool;
}
