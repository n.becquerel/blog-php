<?php

namespace Framework\Container;

use Framework\Parser\YAMLParser;
use InvalidArgumentException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use ReflectionClass;
use ReflectionException;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

/**
 * Class Container
 *
 * @package Framework\Container
 * @author  Nicolas Becquerel <becquerel.nicolas@outlook.fr>
 */
class Container implements ContainerInterface
{
    /**
     * Array of service with ['id' => 'instance']
     *
     * @var array
     */
    private $services = [];

    /**
     * Array of parameters parsed from parameters.yaml
     *
     * @var array
     */
    private $parameters = [];

    private static $container;

    /**
     * @var array
     */
    private $aliases = [];

    /**
     * @var YAMLParser
     */
    private $parser;

    /**
     * Container constructor.
     *
     * @param YAMLParser $parser to parse config in yaml file
     */
    public function __construct(YAMLParser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * Get the existing instance of class, of create a new one
     *
     * @inheritDoc
     */
    public function get($id)
    {
        if ($this->has($id)) {
            return $this->services[$id];
        }

        try {
            $reflector = new ReflectionClass($id);
        } catch (ReflectionException $e) {
            throw new ContainerException(sprintf('Critical error while trying to reflect class %s', $id));
        }

        if ($reflector->isInterface() && isset($this->aliases[$id])) {
            return $this->get($this->aliases[$id]);
        }

        if (!$reflector->isInstantiable()) {
            throw new ContainerException("This class: \"$id\" is not instantiable");
        }

        if ($constructor = $reflector->getConstructor()) {
            $params = $constructor->getParameters();

            $resolvedParams = [];

            foreach ($params as $param) {
                if ($paramClass = $param->getClass()) {
                    $resolvedParams[] = $this->get($paramClass->getName());
                } elseif ($param->isDefaultValueAvailable()) {
                    $resolvedParams[] = $param->getDefaultValue();
                } else {
                    throw new NotFoundException(
                        sprintf(
                            'Class: "%s" not found to make instance of %s',
                            $param->getType(),
                            $id
                        )
                    );
                }
            }

            $this->services[$id] = $reflector->newInstanceArgs($resolvedParams);
            return $this->services[$id];
        }

        $this->services[$id] = $reflector->newInstance();

        return $this->services[$id];
    }

    /**
     * @inheritDoc
     */
    public function has($id)
    {
        if (!is_string($id)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Argument must be a string, %s given in Container::has()',
                    is_object($id) ? get_class($id) : gettype($id)
                )
            );
        }

        if (isset($this->services[$id])) {
            return true;
        }

        return false;
    }

    /**
     * Get the container instance, makes it singleton
     *
     * @return Container
     */
    public static function getInstance()
    {
        if (!self::$container) {
            self::$container = new Container(new YAMLParser());
            self::$container->init();
        }

        return self::$container;
    }

    /**
     * Get a specific parameter parsed from the config file parameters.yaml
     *
     * @param string $id   id of the parameters searched
     *
     * @return mixed|null
     */
    public function getParameter($id)
    {
        if (isset($this->parameters[$id])) {
            return $this->parameters[$id];
        }

        return null;
    }

    /**
     * /!\ OVERRIDE if service already exist /!\
     *
     * @param string $id
     * @param $service
     * @return $this
     */
    public function registerService(string $id, $service = null)
    {
        $this->services[$id] = $service;
        return $this;
    }

    /**
     * Contains special rules to instantiate specific classes
     * Init the container
     */
    public function init()
    {
        $this->registerService(Container::class, $this);

        // register the yaml parser
        $this->registerService(YAMLParser::class, $this->parser);
        $this->parameters = $this->parser->withPath('config')->getContent('parameters');

        // add dynamically services in services.yaml
        $services = $this->parser->withPath('config')->getContent('services');

        if (isset($services['services'])) {
            foreach ($services['services'] as $tag => $class) {
                $this->registerService($tag, $class);
            }
        }

        if (isset($services['interfaces'])) {
            $this->aliases = $services['interfaces'];
        }
    }
}
