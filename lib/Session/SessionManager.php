<?php

namespace Framework\Session;

class SessionManager
{
    /**
     * SessionManager constructor.
     * Start session if not already started
     * Store session vars in this instance
     */
    public function __construct()
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }

        if (isset($_SESSION['flashes']) && isset($_SESSION['flashes']['display'])) {
            unset($_SESSION['flashes']['display']);
        }

        if (isset($_SESSION['flashes']) && isset($_SESSION['flashes']['new'])) {
            $_SESSION['flashes']['display'] = $_SESSION['flashes']['new'];
            unset($_SESSION['flashes']['new']);
        }
    }

    /**
     * @param string $id
     * @return mixed|null
     */
    public function get(string $id)
    {
        if ($this->has($id)) {
            return $_SESSION[$id];
        }

        return null;
    }

    /**
     * Create or override an entry in session
     * @param string $id
     * @param $value
     * @return SessionManager
     */
    public function set(string $id, $value): self
    {
        $_SESSION[$id] = $value;

        return $this;
    }

    /**
     * Test if session has entry
     * @param string $id
     * @return bool
     */
    public function has(string $id): bool
    {
        return isset($_SESSION[$id]);
    }

    /**
     * Remove corresponding entry of session
     * @param string $id
     */
    public function remove(string $id)
    {
        if ($this->has($id)) {
            unset($_SESSION[$id]);
        }
    }

    public function clear()
    {
        session_unset();
    }

    public function getSession()
    {
        return $_SESSION;
    }
}
