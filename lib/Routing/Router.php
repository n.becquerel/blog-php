<?php

namespace Framework\Routing;

use Framework\Container\Container;
use Framework\Parser\YAMLParser;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionException;
use ReflectionParameter;

class Router
{
    /**
     * Routes registered in routes.yaml
     *
     * @var array
     */
    private $routes = [];

    /**
     * Parameters for the matched route
     *
     * @var array
     */
    private $routeParameters = [];

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $parser = YAMLParser::getInstance();
        $this->routes = $parser->withPath('config')->getContent('routes');
    }

    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->callAction($request);
    }

    /**
     * Call the function set in route.yaml
     *
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     * @throws RoutingException
     * @throws ReflectionException
     */
    private function callAction(ServerRequestInterface $request): ResponseInterface
    {
        // instantiation controller en fonction de la route appelée et des paramètres
        $method = $this->matchRoute($request);
//        dd($method, $request);
        $params = explode('::', $method);
        $controllerName = $params[0];
        $controllerMethod = $params[1];

        $controller = Container::getInstance()->get($controllerName);

        $reflector = new \ReflectionMethod($controller, $controllerMethod);

        // get parameters to call method of controller properly
        $methodParams = $reflector->getParameters();

        if (!empty($methodParams)) {
            $callParams = [];
            /** @var ReflectionParameter $param */
            foreach ($methodParams as $param) {
                // si le param est la request envoyer la request
                // sinon, envoyer les args de l'url
                if ($param->name === 'request') {
                    $callParams[] = $request;
                } else {
                    foreach ($this->routeParameters as $routeParameter) {
                        $key = key($routeParameter);
                        if ($param->name === $key) {
                            $callParams[] = $routeParameter[$key];
                        }
                    }
                }
            }

            $response = $reflector->invokeArgs($controller, $callParams);
        } else {
            $response = $reflector->invoke($controller);
        }

        return $response;
    }

    /**
     * Search for a route that match requested url
     * if match, store parameters of the route in instance to invoke the methods with construced params
     *
     * @param ServerRequestInterface $request
     *
     * @return string   the controller and method that must be instantiated/called
     * @throws RoutingException  if no route found
     */
    private function matchRoute(ServerRequestInterface $request): string
    {
        foreach ($this->routes as $route => $params) {
            $url = $request->getUri()->getPath();

            // replace each route params by the corresponding regex, or a global one
            $path = preg_replace_callback(
                '/{([\w]+)}/',
                function ($matches) use ($params) {
                    array_shift($matches);
                    // if regex are defined in routes config file use them
                    // else generic regex
                    if (isset($params['parameters'][$matches[0]])) {
                        $regex = '(' . $params['parameters'][$matches[0]] . ')';
                    } else {
                        $regex = '(\w+|\d+)';
                    }
                    return $regex;
                },
                $route
            );

            $tmpPath = preg_replace('/\//', '\/', $path);
            $regex = "/^$tmpPath$/i";

            if (preg_match($regex, $url, $matches) && in_array($request->getMethod(), $params['methods'])) {
                array_shift($matches);

                if (isset($params['parameters'])) {
                    $i = 0;
                    foreach ($params['parameters'] as $param => $regex) {
                        $this->routeParameters[] = [$param => $matches[$i]];
                        $i++;
                    }
                }

                return $params['action'];
            }
        }

        throw new RoutingException(
            sprintf(
                'No route found for %s',
                $request->getUri()->getPath()
            )
        );
    }

    /**
     * Transform $this->routes in array having the name of the route as key
     * @return array
     */
    public function getNamedRoutes(): array
    {
        $routes = $this->routes;

        $namedRoutes = [];

        foreach ($routes as $path => $config) {
            $namedRoutes[$config['name']] = $path;
        }

        return $namedRoutes;
    }
}
