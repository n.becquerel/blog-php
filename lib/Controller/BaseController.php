<?php

namespace Framework\Controller;

use Exception;
use Framework\Container\Container;
use Framework\Form\Form;
use Framework\Form\FormInterface;
use Framework\Http\RedirectResponse;
use Framework\Http\Response;
use Framework\Http\Stream;
use Framework\Orm\EntityInterface;
use Framework\Routing\Router;
use Framework\Security\Security;
use Framework\Security\SecurityException;
use Framework\Session\SessionManager;
use Psr\Container\ContainerInterface;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

abstract class BaseController
{
    /**
     * The DI container
     *
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Templating engine Twig
     * @var Environment twig environment
     */
    protected $twig;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var Security
     */
    protected $security;

    /**
     * BaseController constructor.
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->container = Container::getInstance();
        $this->security = $this->container->get(Security::class);
        $this->twig = $twig;
    }

    /**
     * Used to render a view with twig and generate response
     *
     * @param string $template
     * @param array $vars
     *
     * @return \Framework\Http\Message|\Psr\Http\Message\MessageInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render(string $template, array $vars = [])
    {
        $stream = new Stream('php://temp', 'r+b');
        $stream->write($this->twig->render($template, $vars));
        return new Response(200, $stream);
    }

    /**
     * Used to create form bound to an entity
     *
     * @param string $form
     * @param $entity
     * @param array $options
     *
     * @return Form
     */
    protected function createForm(string $form, $entity, array $options = []): Form
    {
        $form = $this->container->get($form);

        if ($entity) {
            $form->init($entity);
        }

        $form->buildForm($options);
        return $form;
    }

    /**
     * Construct an url
     *
     * @param string $route
     * @param int $statusCode
     * @param array $parameters
     * @return string
     */
    protected function redirectToRoute(string $route, array $parameters = [], int $statusCode = 302)
    {
        $namedRoutes = $this->container->get(Router::class)->getNamedRoutes();

        if (!isset($namedRoutes[$route])) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Unable to find url for route "%s"',
                    $route
                )
            );
        }

        $url = $namedRoutes[$route];

        $url = preg_replace_callback(
            '/{([\w]+)}/',
            function ($matches) use ($parameters) {
                // to keep only capturing matches
                array_shift($matches);
                return $parameters[$matches[0]];
            },
            $url
        );

        return new RedirectResponse($url, $statusCode);
    }

    /**
     * Add an entry in session with message and type
     * @param string $type
     * @param string $message
     * @param bool $persistThroughNextRequest
     */
    protected function addFlash(string $type, string $message, $persistThroughNextRequest = true): void
    {
        /** @var SessionManager $sessionManager */
        $sessionManager = $this->container->get(SessionManager::class);

        $flashes = [];

        if ($sessionManager->has('flashes')) {
            $flashes = $sessionManager->get('flashes');
        }

        if ($persistThroughNextRequest) {
            $flashes['new'][$type][] = $message;
        } else {
            $flashes['display'][$type][] = $message;
        }

        $sessionManager->set('flashes', $flashes);
    }

    /**
     * If is not admin return a redirectResponse with flash message
     * @return RedirectResponse|bool
     * @throws SecurityException
     */
    protected function requireAdmin()
    {
        if (!$this->security->isGranted('ROLE_ADMIN')) {
            throw new SecurityException();
        }

        return false;
    }
}
