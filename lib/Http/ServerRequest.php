<?php

namespace Framework\Http;

use InvalidArgumentException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;

class ServerRequest extends Request implements ServerRequestInterface
{
    private $serverParams = [];
    private $cookieParams;
    private $uploadedFiles = [];
    private $attributes = [];
    private $parsedBody = [];

    /**
     * ServerRequest constructor.
     * @param string $method
     * @param string|UriInterface $uri
     * @param array $serverParams
     * @param array $headers
     * @param array $cookieParams
     * @param array $uploadedFiles
     * @param string $body
     * @param null $parsedBody
     */
    public function __construct(
        string $method,
        $uri,
        array $serverParams = [],
        $headers = [],
        $cookieParams = [],
        $uploadedFiles = [],
        $body = 'php;//input',
        $parsedBody = null
    ) {
        $this->serverParams = $serverParams;
        $this->cookieParams = $cookieParams;
        $this->uploadedFiles = $uploadedFiles;
        $this->parsedBody = $parsedBody;
        if (is_string($body)) {
            $this->body = new Stream($body, 'r+');
        }

        parent::__construct($uri, $method, $headers);
    }


    /**
     * @inheritDoc
     */
    public function getServerParams()
    {
        return $this->serverParams;
    }

    /**
     * @inheritDoc
     */
    public function getCookieParams()
    {
        return $this->cookieParams;
    }

    /**
     * @inheritDoc
     */
    public function withCookieParams(array $cookies)
    {
        $clone = clone $this;
        $clone->cookieParams = $cookies;
        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function getQueryParams()
    {
        $query = $this->uri->getQuery();

        if (empty($query)) {
            return [];
        }

        $elements = explode('&', $query);

        $queryParams = [];
        foreach ($elements as $element) {
            $values = explode('=', $element);
            $queryParams[$values[0]] = isset($values[1]) ? urldecode($values[1]) : '';
        }

        return $queryParams;
    }

    /**
     * @inheritDoc
     */
    public function withQueryParams(array $query)
    {
        $clone = clone $this;
        $clone->queryParams = $query;
        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function getUploadedFiles()
    {
        return $this->uploadedFiles;
    }

    /**
     * @inheritDoc
     */
    public function withUploadedFiles(array $uploadedFiles)
    {
        $clone = clone $this;
        $clone->uploadedFiles = $uploadedFiles;
        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function getParsedBody()
    {
        return $this->parsedBody;
    }

    /**
     * @inheritDoc
     */
    public function withParsedBody($data)
    {
        if (!is_array($data) || $data != null) {
            throw new InvalidArgumentException(
                sprintf(
                    'Body must be array or null %s given.',
                    gettype($data)
                )
            );
        }

        $clone = clone $this;
        $clone->body = $data;
        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @inheritDoc
     */
    public function getAttribute($name, $default = null)
    {
        if (!isset($this->attributes[$name])) {
            return $default;
        }
        return $this->attributes[$name];
    }

    /**
     * @inheritDoc
     */
    public function withAttribute($name, $value)
    {
        $clone = clone $this;
        $clone->attributes[$name] = $value;
    }

    /**
     * @inheritDoc
     */
    public function withoutAttribute($name)
    {
        $clone = clone $this;
        unset($clone->attributes[$name]);
        return $clone;
    }
}
