<?php

namespace Framework\Http;

use InvalidArgumentException;
use Psr\Http\Message\StreamInterface;
use RuntimeException;

class Stream implements StreamInterface
{
    /**
     * @var resource
     */
    protected $stream;

    /**
     * Stream constructor.
     * @param  $stream
     * @param string $mode
     */
    public function __construct($stream, $mode = 'r')
    {
        $this->setStream($stream, $mode);
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        try {
            $this->rewind();
            $content = $this->getContents();
        } catch (RuntimeException $e) {
            $content = '';
        }

        return $content;
    }

    /**
     * @inheritDoc
     */
    public function close()
    {
        if (!$this->stream) {
            return;
        }

        fclose($this->detach());
    }

    /**
     * @inheritDoc
     */
    public function detach()
    {
        $stream = $this->stream;
        $this->stream = null;
        return $stream;
    }

    /**
     * @inheritDoc
     */
    public function getSize()
    {
        return fstat($this->stream)['size'] ?? null;
    }

    /**
     * @inheritDoc
     */
    public function tell()
    {
        $position = ftell($this->stream);

        if (!is_int($position)) {
            throw new RuntimeException('Can\'t find pointer\'s position.');
        }

        return $position;
    }

    /**
     * @inheritDoc
     */
    public function eof()
    {
        return feof($this->stream);
    }

    /**
     * @inheritDoc
     */
    public function isSeekable()
    {
        return stream_get_meta_data($this->stream)['seekable'];
    }

    /**
     * @inheritDoc
     */
    public function seek($offset, $whence = SEEK_SET)
    {
        $fseek = fseek($this->stream, $offset, $whence);

        if ($fseek === -1) {
            throw new RuntimeException('Unable to seek to offset for stream.');
        }
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        if (!$this->isSeekable()) {
            throw new RuntimeException('Stream not seekable.');
        }

        $this->seek(0);
    }

    /**
     * @inheritDoc
     */
    public function isWritable()
    {
        $mode = $this->getMetadata('mode');

        return  (
            strstr($mode, 'w') ||
            strstr($mode, 'a') ||
            strstr($mode, 'x') ||
            strstr($mode, 'c') ||
            strstr($mode, '+')
        );
    }

    /**
     * @inheritDoc
     */
    public function write($string)
    {
        if (!$this->isWritable()) {
            throw new RuntimeException('Unable to write in stream.');
        }

        return fwrite($this->stream, $string);
    }

    /**
     * @inheritDoc
     */
    public function isReadable()
    {
        $mode = stream_get_meta_data($this->stream)['mode'];

        return (
            strstr($mode, 'r') ||
            strstr($mode, '+')
        );
    }

    /**
     * @inheritDoc
     */
    public function read($length)
    {
        if (!$this->isReadable()) {
            throw new RuntimeException('Stream not readable.');
        }

        return fread($this->stream, $length) ?? '';
    }

    /**
     * @inheritDoc
     */
    public function getContents()
    {
        $content = stream_get_contents($this->stream);

        if (!$content) {
            throw new RuntimeException('Unable to read the stream.');
        }

        return $content;
    }

    /**
     * @inheritDoc
     */
    public function getMetadata($key = null)
    {
        $streamData = stream_get_meta_data($this->stream);

        if ($key) {
            return stream_get_meta_data($this->stream)[$key] ?? null;
        }

        return $streamData;
    }

    /**
     * @param $stream
     * @param string $mode
     */
    private function setStream($stream, string $mode)
    {
        $resource = null;

        if (is_string($stream)) {
            $resource = fopen($stream, $mode);
        }

        if (!is_resource($resource) || get_resource_type($resource) !== 'stream') {
            throw new InvalidArgumentException(
                'Invalid stream provided, must be a string stream identifier or stream resource'
            );
        }

        $this->stream = $resource;
    }
}
