<?php

namespace Framework\Http;

use InvalidArgumentException;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;

class UriFactory implements UriFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createUri(string $uri = ''): UriInterface
    {
        $parsedUri = parse_url($uri);

        if (!$parsedUri) {
            throw new InvalidArgumentException('Invalid arguments, impossible to instantiate Uri');
        }

        return new Uri($uri);
    }
}
