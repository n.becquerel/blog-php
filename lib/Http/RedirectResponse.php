<?php


namespace Framework\Http;

class RedirectResponse extends Response
{
    public function __construct($route = 'home', int $statusCode = 301, string $reasonPhrase = '')
    {
        $headers['location'] = [$route];

        parent::__construct($statusCode, 'php://temp', $headers, $reasonPhrase);
    }
}
