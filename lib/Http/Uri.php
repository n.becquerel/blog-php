<?php

namespace Framework\Http;

use Psr\Http\Message\UriInterface;

class Uri implements UriInterface
{
    /**
     * @var string
     */
    private $scheme;

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $port;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $pass;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $query;

    /**
     * @var string
     */
    private $fragment;

    private const DEFAULT_PORT = [
        'http' => 80,
        'https' => 443,
        'ftp' => 21,
        'gopher' => 70,
        'nntp' => 119,
        'news' => 119,
        'telnet' => 23,
        'tn3270' => 23,
        'imap' => 143,
        'pop' => 110,
        'ldap' => 389,
    ];

    public function __construct($uri = '')
    {
        $uri = parse_url($uri);

        if (!$uri) {
            throw new \InvalidArgumentException(sprintf('Invalid arguments to instantiate Uri'));
        }

        $this->scheme = isset($uri['scheme']) ? $uri['scheme'] : '';
        $this->host = isset($uri['host']) ? $uri['host'] : '';
        $this->port = isset($uri['port']) ? $uri['port'] : null;
        $this->user = isset($uri['user']) ? $uri['user'] : '';
        $this->pass = isset($uri['pass']) ? $uri['pass'] : '';
        $this->path = isset($uri['path']) ? $uri['path'] : '';
        $this->query = isset($uri['query']) ? $uri['query'] : '';
        $this->fragment = isset($uri['fragment']) ? $uri['fragment'] : '';
    }

    /**
     * @inheritDoc
     */
    public function getScheme()
    {
        $scheme = $this->scheme;

        if (strpos(':', $scheme) != false) {
            $scheme = preg_replace(':', '', $scheme);
        }

        return strtolower($scheme);
    }

    /**
     * @inheritDoc
     */
    public function getAuthority()
    {
        $authority = $this->getUserInfo() . '@' . $this->host;

        if (!$this->isDefaultPort() || !isset($this->port)) {
            $authority .= $this->port;
        }

        return $authority;
    }

    /**
     * @inheritDoc
     */
    public function getUserInfo()
    {
        $userInfo = $this->user;

        if (!empty($this->pass)) {
            $userInfo .= ':' . $this->pass;
        }

        return $userInfo;
    }

    /**
     * @inheritDoc
     */
    public function getHost()
    {
        return strtolower($this->host);
    }

    /**
     * @inheritDoc
     */
    public function getPort()
    {
        if ($this->port) {
            if ($this->isDefaultPort()) {
                $port = null;
            } else {
                $port = $this->port;
            }
        } else {
            if ($this->scheme) {
                $port = self::DEFAULT_PORT[$this->scheme];
            } else {
                $port = null;
            }
        }
        return $port;
    }

    /**
     * @inheritDoc
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @inheritDoc
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @inheritDoc
     */
    public function getFragment()
    {
        return $this->fragment;
    }

    /**
     * @inheritDoc
     */
    public function withScheme($scheme)
    {
        if (!is_string($scheme)) {
            throw new \InvalidArgumentException(sprintf(
                'Uri path must be string, %s given',
                gettype($scheme)
            ));
        }

        $clone = clone $this;
        $clone->scheme = $scheme;

        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function withUserInfo($user, $password = null)
    {
        $clone = clone $this;
        $clone->user = $user;
        $clone->pass = $password ? $password : '';

        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function withHost($host)
    {
        if (!is_string($host)) {
            throw new \InvalidArgumentException(sprintf(
                'Uri host must be string, %s given',
                gettype($host)
            ));
        }

        $clone = clone $this;
        $clone->host = $host;
        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function withPort($port)
    {
        if (!is_int($port) && !$port) {
            throw new \InvalidArgumentException(sprintf(
                'uri port must be int or null, %s given',
                gettype($port)
            ));
        }

        $clone = clone $this;
        $clone->port = $port;
        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function withPath($path)
    {
        if (!is_string($path)) {
            throw new \InvalidArgumentException(sprintf(
                'Uri path must be string, %s given',
                gettype($path)
            ));
        }

        $clone = clone $this;
        $clone->path = $path;
        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function withQuery($query)
    {
        if (!is_string($query)) {
            throw new \InvalidArgumentException(sprintf(
                'Uri host must be string, %s given',
                gettype($query)
            ));
        }

        $clone = clone $this;
        $clone->query = $query;
        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function withFragment($fragment)
    {
        $clone = clone $this;
        $clone->fragment = $fragment;
        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        $userInfos = $this->getUserInfo();
        if (!empty($userInfos)) {
            $userInfos = '//' . $userInfos . '@';
        }

        $port = $this->getPort();
        if (!empty($port)) {
            $port = ':' . $port;
        }

        $query = $this->getQuery();
        if (!empty($query)) {
            $query = '?' . $query;
        }

        $fragment = $this->getFragment();
        if (!empty($fragment)) {
            $fragment = '#' . $fragment;
        }

        return $this->getScheme() . ':' . $userInfos . $this->getHost() . $port . $this->getPath() . $query . $fragment;
    }

    /**
     * Condition if this port is default port for this scheme
     * @return bool
     */
    private function isDefaultPort()
    {
        if (self::DEFAULT_PORT[$this->scheme] === $this->port) {
            return true;
        }

        return false;
    }
}
