<?php

namespace Framework\Http;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;

/**
 * Class Request
 * @package Framework\Http
 * @inheritDoc
 */
class Request extends Message implements RequestInterface
{
    /**
     * Common http request methods
     */
    private const REQUEST_METHODS = [
        'post' => 'POST',
        'get' => 'GET',
        'put' => 'PUT',
        'patch' => 'PATCH',
        'delete' => 'DELETE'
    ];

    /**
     * @var Uri
     */
    protected $uri;

    /**
     * @var string
     */
    private $requestTarget;

    /**
     * @var string
     */
    protected $method;

    /**
     * Request constructor.
     * @param string|UriInterface $uri
     * @param string $method
     * @param array $headers
     */
    public function __construct($uri, $method, $headers = [])
    {
        if (!$uri instanceof UriInterface && is_string($uri)) {
            $uri = new Uri($uri);
        }

        $this->uri = $uri;
        $this->setHeaders($headers);
        $this->method = $method;
    }

    /**
     * @inheritDoc
     */
    public function getRequestTarget()
    {
        if (!empty($this->requestTarget)) {
            return $this->requestTarget;
        }

        $target = '/';

        if (!empty($path = $this->uri->getPath())) {
            $target = $path;

            if (!empty($query = $this->uri->getQuery())) {
                $target .= '?' . $query;
            }
        }

        return $target;
    }

    /**
     * @inheritDoc
     */
    public function withRequestTarget($requestTarget)
    {
        $clone = clone $this;
        $clone->requestTarget = $requestTarget;
        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @inheritDoc
     */
    public function withMethod($method)
    {
        if (!isset(self::REQUEST_METHODS[strtolower($method)])) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Http method not found, %s, %s given',
                    gettype($method),
                    $method
                )
            );
        }

        $clone = clone $this;
        $clone->method = $method;
        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @inheritDoc
     */
    public function withUri(UriInterface $uri, $preserveHost = false)
    {
        $clone = clone $this;
        $clone->uri = $uri;

        if (!empty($uri->getHost()) || ($preserveHost && !$clone->hasHeader('host'))) {
            $clone->updateHost();
        }

        return $clone;
    }

    /**
     * Updating $this header['Host']
     */
    private function updateHost()
    {
        $host = $this->uri->getHost();

        if (empty($host)) {
            return;
        }

        if (!empty($port = $this->uri->getPort())) {
            $host .= ':' . $port;
        }

        if (!isset($this->normalizedHeadersName['host'])) {
            $this->normalizedHeadersName['host'] = [
                'Host'
            ];
        }

        $this->headers = ['Host' => [$host]] + $this->headers;
    }
}
