<?php

namespace Framework\Http;

use Framework\Container\Container;
use Framework\Routing\Router;

class UrlGenerator
{
    /**
     * Create and return an url with parameters
     *
     * @param string $namedRoute
     * @param array $parameters
     * @return string|string[]|null
     */
    public function generateUrl(string $namedRoute, array $parameters = [])
    {
        $namedRoutes = Container::getInstance()->get(Router::class)->getNamedRoutes();

        if (!isset($namedRoutes[$namedRoute])) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Unable to find url for route "%s"',
                    $namedRoute
                )
            );
        }

        $url = $namedRoutes[$namedRoute];

        $url = preg_replace_callback(
            '/{([\w]+)}/',
            function ($matches) use ($parameters) {
                array_shift($matches);
                return $parameters[$matches[0]];
            },
            $url
        );

        return $url;
    }
}
