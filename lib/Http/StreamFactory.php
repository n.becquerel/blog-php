<?php

namespace Framework\Http;

use Framework\Http\Stream;
use InvalidArgumentException;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;
use RuntimeException;

class StreamFactory implements StreamFactoryInterface
{
    private const MODES = [
        'r',
        'r+',
        'w',
        'w+',
        'a',
        'a+',
        'x',
        'x+',
        'c',
        'c+',
        'e'
    ];

    /**
     * @inheritDoc
     */
    public function createStream(string $content = ''): StreamInterface
    {
        return new Stream($content);
    }

    /**
     * @inheritDoc
     */
    public function createStreamFromFile(string $filename, string $mode = 'r'): StreamInterface
    {
        if (!isset(self::MODES[$mode])) {
            throw new InvalidArgumentException(
                sprintf(
                    'Mode : %s, is not allowed mode.',
                    $mode
                )
            );
        }

        if (is_resource(fopen($filename, $mode))) {
            throw new RuntimeException('File is not readable.');
        }

        return new Stream($filename, $mode);
    }

    /**
     * @inheritDoc
     */
    public function createStreamFromResource($resource): StreamInterface
    {
        return new Stream($resource);
    }
}
