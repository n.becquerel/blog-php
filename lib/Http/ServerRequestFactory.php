<?php

namespace Framework\Http;

use Framework\Http\ServerRequest;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;

class ServerRequestFactory implements ServerRequestFactoryInterface
{

    /**
     * @inheritDoc
     */
    public function createServerRequest(string $method, $uri, array $serverParams = []): ServerRequestInterface
    {
        return new ServerRequest($method, $uri, $serverParams);
    }

    /**
     * Create a ServerRequest from php $_SERVER Global
     *
     * @return ServerRequest
     */
    public function createFromGlobals(): ServerRequest
    {
        $uri = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http';

        $uri .= "://";

        $uri .= $_SERVER['HTTP_HOST'];

        $uri .= $_SERVER['REQUEST_URI'];

        $headers = [];

        if (!function_exists('getallheaders')) {
            function getallheaders()
            {
                $headers = [];
                foreach ($_SERVER as $name => $value) {
                    if (substr($name, 0, 5) == 'HTTP_') {
                        $headers[str_replace(
                            ' ',
                            '-',
                            ucwords(strtolower(str_replace(
                                '_',
                                ' ',
                                substr($name, 5)
                            )))
                        )] = $value;
                    }
                }
                return $headers;
            }
        } else {
            $headers = getallheaders();
        }

        return new ServerRequest(
            $_SERVER['REQUEST_METHOD'],
            $uri,
            $_SERVER,
            $headers,
            $_COOKIE,
            $_FILES,
            'php://input',
            $_POST
        );
    }
}
