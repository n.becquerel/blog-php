<?php

namespace Framework\Http;

use InvalidArgumentException;
use phpDocumentor\Reflection\Types\This;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UploadedFileInterface;
use RuntimeException;

class UploadedFile implements UploadedFileInterface
{
    private const ERROR_MESSAGES = [
        UPLOAD_ERR_OK         => 'There is no error, the file uploaded with success',
        UPLOAD_ERR_INI_SIZE   => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
        UPLOAD_ERR_FORM_SIZE  => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was '
            . 'specified in the HTML form',
        UPLOAD_ERR_PARTIAL    => 'The uploaded file was only partially uploaded',
        UPLOAD_ERR_NO_FILE    => 'No file was uploaded',
        UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder',
        UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk',
        UPLOAD_ERR_EXTENSION  => 'A PHP extension stopped the file upload.',
    ];

    /** @var StreamInterface */
    private $stream;

    /**
     * @var int
     */
    private $size;

    /**
     * @var int
     */
    private $error;

    /**
     * @var string
     */
    private $clientFileName;

    /**
     * @var string
     */
    private $clientMediaType;

    /**
     * @var bool
     */
    private $moved;

    public function __construct(
        StreamInterface $stream,
        int $size = null,
        int $error = \UPLOAD_ERR_OK,
        string $clientFilename = null,
        string $clientMediaType = null
    ) {
        $this->stream = $stream;
        $this->size = $size;
        $this->error = $error;
        $this->clientFileName = $clientFilename;
        $this->clientMediaType = $clientMediaType;
    }

    /**
     * @inheritDoc
     */
    public function getStream()
    {
        if ($this->moved) {
            throw new RuntimeException('File moved, cant get stream.');
        }

        return $this->stream;
    }

    /**
     * @inheritDoc
     */
    public function moveTo($targetPath)
    {
        if ($this->moved) {
            throw new RuntimeException('File already moved.');
        }

        if ($this->error !== UPLOAD_ERR_OK) {
            throw new RuntimeException(
                self::ERROR_MESSAGES[$this->error]
            );
        }

        if (!is_string($targetPath) || empty($targetPath)) {
            throw new InvalidArgumentException(
                'TargetPath must be a non-empty string.'
            );
        }

        $targetDirectory = dirname($targetPath);
        if (!is_dir($targetDirectory) || !is_writable($targetDirectory)) {
            throw new RuntimeException(
                'TargetPath is not a directory, or is not allowed to write into.'
            );
        }

        $sapi = PHP_SAPI;
        switch (true) {
            case (empty($sapi) || 0 === strpos($sapi, 'cli') || 0 === strpos($sapi, 'phpdbg') || !$this->stream):
                // Non-SAPI environment, or no filename present
                $resource = new Stream($targetPath, 'w');
                $resource->write($this->stream->getContents());
                break;
            default:
                // SAPI environment, with file present
                if (false === move_uploaded_file($this->stream, $targetPath)) {
                    throw new RuntimeException('File could not move');
                }
                break;
        }

        $this->moved = true;
    }

    /**
     * @inheritDoc
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @inheritDoc
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @inheritDoc
     */
    public function getClientFilename()
    {
        return $this->clientFileName;
    }

    /**
     * @inheritDoc
     */
    public function getClientMediaType()
    {
        return $this->clientMediaType;
    }
}
