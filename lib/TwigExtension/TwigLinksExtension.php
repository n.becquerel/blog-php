<?php

namespace Framework\TwigExtension;

use Framework\Http\UriFactory;
use Framework\Routing\Router;
use InvalidArgumentException;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigLinksExtension extends AbstractExtension
{
    /**
     * @var UriFactory
     */
    private $uriFactory;

    /**
     * @var array
     */
    private $namedRoutes;

    /**
     * TwigLinksExtension constructor.
     * @param UriFactory $uriFactory
     * @param Router $router
     */
    public function __construct(UriFactory $uriFactory, Router $router)
    {
        $this->uriFactory = $uriFactory;
        $this->namedRoutes = $router->getNamedRoutes();
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('path', [$this, 'createUrl']),
            new TwigFunction('asset', [$this, 'getAssetLink']),
        ];
    }

    /**
     * Generate uri with the name of the route
     *
     * @param string $name
     * @param array $params
     * @return \Psr\Http\Message\UriInterface
     */
    public function createUrl(string $name, array $params = []): string
    {
        if (!is_string($name)) {
            throw new InvalidArgumentException(sprintf('Name of the route must be of type string'));
        }

        if (!isset($this->namedRoutes[$name])) {
            throw new InvalidArgumentException(
                sprintf(
                    'No url found for "%s", check routes.yaml file',
                    $name
                )
            );
        }

        return $this->replaceRouteParameterWithValue($this->namedRoutes[$name], $params);
    }

    /**
     * Return the path to assets folder
     * @param string $path
     * @return string
     */
    public function getAssetLink(string $path): string
    {
        return '/assets/' . $path;
    }

    /**
     * Replace all routes parameters with the value passed as param
     * @param string $route
     * @param array $params
     * @return string
     */
    private function replaceRouteParameterWithValue(string $route, array $params)
    {
        if (!preg_match('/{[\w]+}/', $route)) {
            return $route;
        }

        $route = preg_replace_callback(
            '/{([\w]+)}/',
            function ($matches) use ($params) {
                // to keep only capturing matches
                array_shift($matches);

                foreach ($matches as $match) {
                    if (isset($params[$match])) {
                        return $params[$match];
                    } else {
                        throw new InvalidArgumentException(
                            sprintf(
                                'Impossible to generate a route, parameter "%s" not given to method path().',
                                $match
                            )
                        );
                    }
                }
            },
            $route
        );

        return $route;
    }
}
