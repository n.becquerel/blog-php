<?php

namespace Framework\TwigExtension;

use Framework\Security\UserInterface;
use Framework\Session\SessionManager;

class TwigSecurityExtension
{
    /**
     * @var SessionManager
     */
    private $session;

    /**
     * TwigSecurityExtension constructor.
     * @param SessionManager $sessionManager
     */
    public function __construct(SessionManager $sessionManager)
    {
        $this->session = $sessionManager;
    }

//    public function getUser(): UserInterface
//    {
//
//    }
//
//    public function is_granted()
//    {
//
//    }
}
