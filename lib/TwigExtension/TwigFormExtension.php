<?php

namespace Framework\TwigExtension;

use Framework\Form\FormElement;
use Framework\Form\HtmlElement;
use Framework\Orm\EntityInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class TwigFormExtension
 * Used to display html form
 *
 * @package Framework\TwigExtension
 */
class TwigFormExtension extends AbstractExtension
{
    private const SPECIAL_INPUTS = [
        'textarea' => 'createTextArea',
        'wysiwyg' => 'createTextAreaForWysiwyg',
        'entity' => 'createSelect',
        'choice' => 'createSelect'
    ];

    public function getFunctions()
    {
        return [
            new TwigFunction('form_widget', [$this, 'printFormWidget']),
            new TwigFunction('form_label', [$this, 'printFormLabel']),
            new TwigFunction('form_error', [$this, 'printFormError']),
            new TwigFunction('form_errors', [$this, 'printFormErrors'])
        ];
    }

    /**
     * Echo an html input
     *
     * @param FormElement $formElement
     */
    public function printFormWidget(FormElement $formElement)
    {
        if (!empty($formElement->getError()->getMessages())) {
            if ($formElement->getInput()->getAttrs()) {
                $formElement->getInput()->addAttr(['class' => 'is-invalid']);
            }
        }

        $method = isset(self::SPECIAL_INPUTS[$formElement->getInput()->getType()]) ?
            self::SPECIAL_INPUTS[$formElement->getInput()->getType()] : 'createInput';
        echo $this->$method($formElement);
    }

    /**
     * Echo an html form label
     *
     * @param FormElement $formElement
     */
    public function printFormLabel(FormElement $formElement)
    {
        $labelElement = $formElement->getLabel();

        $label = '<label ';

        $label .= 'for="' . $labelElement->getFor() . '" ';

        $label .= $this->addAttrs($labelElement);

        $label .= '>' . $labelElement->getValue() . '</label>';

        echo $label;
    }

    /**
     * Add the html attrs to a widget (input, label etc...)
     *
     * @param HtmlElement $elem
     * @return string
     */
    private function addAttrs(HtmlElement $elem): string
    {
        $str = '';

        foreach ($elem->getAttrs() as $name => $value) {
            $str .= $name . '="' . $value . '" ';
        }

        return $str;
    }

    /**
     * Create textarea widget
     *
     * @param FormElement $formElement
     * @param bool $escaped
     * @return string
     */
    private function createTextArea(FormElement $formElement, bool $escapeValue = true): string
    {
        $input = $formElement->getInput();

        $widget = '<textarea ';

        $widget .= 'id="' . $input->getId() . '" ';

        $widget .= 'name="' . $input->getName() . '" ';

        $widget .= $this->addAttrs($input);

        $value = $escapeValue ? htmlspecialchars($input->getValue()) : $input->getValue();

        $widget .= '>' . $value . '</textarea>';

        return $widget;
    }

    public function createTextAreaForWysiwyg(FormElement $formElement)
    {
        return $this->createTextArea($formElement, false);
    }

    /**
     * Create select widget
     *
     * @param FormElement $formElement
     * @return string
     */
    private function createSelect(FormElement $formElement): string
    {
        $input = $formElement->getInput();

        $widget = '<select ';

        $widget .= 'name="' . $input->getName() . '" ';

        $widget .= 'id="' . $input->getId() .  '" ';

        $widget .= $this->addAttrs($input) . '>';

        if (isset($input->getGlobals()['options']['placeholder'])) {
            $widget .= '<option value="" >' . $input->getGlobals()['options']['placeholder'] . '</option>';
        }

        foreach ($input->getChoices() as $value => $item) {
            if ($item instanceof EntityInterface) {
                $value = $item->getId();
            }

            $choiceLabel = $item;

            if (isset($input->getGlobals()['options']['choice_label'])) {
                $getter = 'get' . $input->getGlobals()['options']['choice_label'];
                $choiceLabel = $item->$getter();
            }

            $widget .= '<option value="' . $value . '" ';

            if ($input->getValue() instanceof EntityInterface) {
                $widget .= $value === $input->getValue()->getId() ? 'selected' : '';
            } else {
                $widget .= $value === $input->getValue() ? 'selected' : '';
            }

            $widget .= '>' . $choiceLabel . '</option>';
        }

        $widget .= '</select>';

        return $widget;
    }

    /**
     * Create basic widget
     *
     * @param FormElement $formElement
     * @return string
     */
    private function createInput(FormElement $formElement): string
    {
        $input = $formElement->getInput();

        $widget = '<input ';

        $widget .= 'type="' . $input->getType() . '" ';

        $widget .= 'id="' . $input->getId() . '" ';

        $widget .= 'name="' . $input->getName() . '" ';

        $widget .= $this->addAttrs($input);

        $widget .= 'value="' . htmlentities($input->getValue()) . '" ';

        $widget .= '/>';

        return $widget;
    }

    /**
     * Print all form errors
     *
     * @param FormElement[] $formView
     */
    public function printFormErrors(array $formView)
    {
        $widget = '<span class="alert alert-danger"';

        $emptyErrors = true;
        foreach ($formView as $formElement) {
            $emptyErrors = empty($formElement->getError()->getMessages());
            $errors = $this->addErrorMessage($formElement);
        }

        $widget .= $emptyErrors ? ' style="display: none;"' : '';

        $widget .= '"><ul class="list-unstyled m-0">';

        $widget .= $errors ?? '';

        $widget .= '</ul></span>';

        echo $widget;
    }

    /**
     * Echo error messages for a given formElement
     *
     * @param FormElement $formElement
     */
    public function printFormError(FormElement $formElement)
    {
        $widget = '<span class="badge badge-danger"';

        $widget .= empty($formElement->getError()->getMessages()) ? ' style="display: none;"' : '';

        $widget .= '><ul class="list-unstyled m-0">';

        $widget .= $this->addErrorMessage($formElement);

        $widget .= '</ul></span>';

        echo $widget;
    }

    /**
     * Loop over errors messages of formElement and add the to str
     *
     * @param FormElement $formElement
     * @return string
     */
    private function addErrorMessage(FormElement $formElement)
    {
        $str = '';
        foreach ($formElement->getError()->getMessages() as $error) {
            $str .= '<li>' . $error . '</li>';
        }
        return $str;
    }
}
