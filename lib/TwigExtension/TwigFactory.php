<?php

namespace Framework\TwigExtension;

use Framework\Container\Container;
use Framework\Parser\YAMLParser;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Class TwigFactory
 * Contains special rules to create twig instance
 *
 * @package Framework\TwigExtension
 */
class TwigFactory
{
    /**
     * Instantiate twig with extensions, function etc...
     *
     * @return Environment
     */
    public static function createTwig(): Environment
    {
        $container = Container::getInstance();
        $parser = $container->get(YAMLParser::class);
        $globalConfig = $parser->withPath('config')->getContent('services');
        $config = $globalConfig['twig'];

        $twigLoader = new FilesystemLoader($config['base_path']);
        $twig = new Environment($twigLoader, $config['options']);

        if (isset($config['extensions'])) {
            foreach ($config['extensions'] as $class) {
                $twig->addExtension($container->get($class));
            }
        }

        if (isset($config['services'])) {
            foreach ($config['services'] as $serviceName => $class) {
                $twig->addGlobal($serviceName, $container->get($class));
            }
        }

        if (isset($config['globals'])) {
            foreach ($config['globals'] as $global => $value) {
                $twig->addGlobal($global, $value);
            }
        }

        return $twig;
    }
}
