<?php

namespace Framework\Core;

use Framework\Container\Container;
use Framework\Http\Response;
use Framework\Http\ServerRequestFactory;
use Framework\Http\Stream;
use Framework\ResponseHandler;
use Framework\Routing\Router;
use Framework\Security\SecurityException;
use Framework\TwigExtension\TwigFactory;
use Psr\Http\Message\ResponseInterface;
use Twig\Environment;

class App
{
    const INTERNAL_ERROR = 1;

    const SECURITY_ERROR = 2;

    const NOT_FOUND_ERROR = 3;

    public static function init(): void
    {
        $container = Container::getInstance();

        try {
            $twig = TwigFactory::createTwig();
            $container->registerService(Environment::class, $twig);
        } catch (\Exception $exception) {
            if ($container->getParameter('env') === 'PROD') {
                self::handleError(self::INTERNAL_ERROR);
            }
        }
    }

    public static function run(): ?ResponseInterface
    {
        try {
            $serverRequestFactory = new ServerRequestFactory();
            $serverRequest = $serverRequestFactory->createFromGlobals();

            $router = new Router();
            $response = $router->handle($serverRequest);

            if ($response->getStatusCode() === 404) {
                self::handleError(self::NOT_FOUND_ERROR);
            }

            return $response;
        } catch (\Exception $e) {
            if (get_class($e) === SecurityException::class) {
                self::handleError(self::SECURITY_ERROR);
            }

            if (Container::getInstance()->getParameter('env') === 'PROD') {
                self::handleError(self::INTERNAL_ERROR);
            }

            return null;
        }
    }

    public static function handleResponse(ResponseInterface $response): void
    {
        $responseHandler = new ResponseHandler();
        $responseHandler->handleResponse($response);
    }

    private static function handleError(int $errorCode)
    {
        $container = Container::getInstance();
        $response = new Response();

        switch ($errorCode) {
            case self::SECURITY_ERROR:
                $template = $container->getParameter('401page') ?? 'errors/_security.html.twig';
                $body = self::createBodyWithTemplate($template);
                $response = $response->withStatus(401)->withBody($body);
                break;
            case self::NOT_FOUND_ERROR:
                $template = $container->getParameter('404page') ?? 'errors/_404.html.twig';
                $body = self::createBodyWithTemplate($template);
                $response = $response->withStatus(404)->withBody($body);
                break;
            case self::INTERNAL_ERROR:
            default:
                $template = $container->getParameter('500page') ?? 'errors/_500.html.twig';
                $body = self::createBodyWithTemplate($template);
                $response = $response->withStatus(500)->withBody($body);
                break;
        }

        self::handleResponse($response);
    }

    private static function createBodyWithTemplate(string $template): Stream
    {
        $body = new Stream('php://temp', 'r+b');
        $twig = Container::getInstance()->get(Environment::class);
        $body->write($twig->render($template));
        return $body;
    }
}
