<?php

namespace Framework\Orm;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Framework\Container\Container;
use Framework\Parser\YAMLParser;
use ReflectionClass;

/**
 * Class EntityRepository used to provide common methods to each entity
 * Provide methods to read data from database
 * and insert, update, delete in db
 * @package Framework\Orm
 */
class EntityRepository
{
    /**
     * @var Database
     */
    private $db;

    /**
     * @var string
     */
    public $className;

    /**
     * @var string
     */
    private $tableName;

    /**
     * @var array
     */
    private $fields = [];

    public function __construct(YAMLParser $parser, Database $db, $className)
    {
        $this->db = $db;

        $class = explode('\\', $className);
        // place where config files needs to stand
        $metadata = $parser->withPath('src/Entity/config')->getContent(end($class));

        $this->processMetaData($metadata);
    }

    /**
     * Process the metadata array to set class properties
     * @param array $metadata
     */
    private function processMetaData(array $metadata)
    {
        $this->className = $metadata['class_name'];
        $this->tableName = $metadata['table'];
        $this->fields = $metadata['fields'];
    }

    /**
     * Find abject by id
     * @param $id
     * @param array $parsedEntityRelation
     * @return EntityInterface|null
     * @throws OrmNotFoundException
     */
    public function find($id, array $parsedEntityRelation = [])
    {
        $q = $this->createQueryBuilder()
            ->where('id = :id')
            ->setParameters(['id' => $id])
            ->getQuery()->getSingleResult();

        if (!$q) {
            return null;
        }

        return $this->hydrateObject($q, $parsedEntityRelation);
    }

    /**
     * Find all records for an entity in database
     *
     * @param array $parsedEntityRelation
     * @return array|null
     * @throws OrmNotFoundException
     */
    public function findAll(array $parsedEntityRelation = []): array
    {
        $q = $this->createQueryBuilder()
            ->getQuery();

        $result = $q->getResult();

        return $this->hydrateMany($result, $parsedEntityRelation);
    }

    /**
     * @param array $fields
     * @param array $parsedEntityRelation
     * @return array|null
     * @throws OrmNotFoundException
     */
    public function findBy(array $fields, array $parsedEntityRelation = [])
    {
        $q = $this->createQueryBuilder();

        $i = 0;
        foreach ($fields as $field => $value) {
            foreach ($this->fields as $property => $values) {
                if ($values['name'] === $field) {
                    $propertyName = $property;

                    if (isset($propertyName)) {
                        if ($i === 0) {
                            $q->where($q->getAlias() . '.' . $propertyName . ' = :' . $field);
                        } else {
                            $q->andWhere($q->getAlias() . '.' . $propertyName . ' = :' . $field);
                        }
                    } else {
                        throw new OrmNotFoundException(
                            sprintf(
                                'Could not find "%s" field in table "%s", please check the entity config file.',
                                $field,
                                $this->tableName
                            )
                        );
                    }
                }
            }

            $i++;
        }

        $q->setParameters($fields)
            ->getQuery();
        $result = $q->getResult();

        return $this->hydrateMany($result, $parsedEntityRelation);
    }

    /**
     * perform insert in db with prepared statement
     *
     * @param EntityInterface $entity
     *
     * @return string
     * @throws \ReflectionException
     */
    public function insert(EntityInterface $entity)
    {
        $sql = "INSERT INTO $this->tableName (";
        $reflector = new ReflectionClass($this->className);

        // values of each field to perform the insert after the loop
        $values = [];

        foreach ($properties = $reflector->getProperties() as $index => $propertie) {
            $getter = 'get' . $propertie->getName();
            $value = $entity->$getter();

            $canAdd = true;

            if (is_array($value)) {
                $canAdd = !$value[0] instanceof EntityInterface;
            }

            if ($canAdd) {
                $config = $this->getField($propertie->getName());

                if ($value) {
                    if ($value instanceof DateTimeInterface) {
                        $value = $value->format('Y-m-d');
                    } elseif ($value instanceof EntityInterface) {
                        $value = $value->getId();
                    } elseif ($config['config']['type'] === 'array') {
                        $value = serialize($value);
                    }

                    $values[$config['db_name']] = $value;
                }
            }
        }

        $sql .= implode(', ', array_keys($values));

        $sql .= ') VALUES (';

        $sql .= implode(
            ', ',
            array_map(function ($elem) {
                return ':' . $elem;
            }, array_keys($values))
        );

        $sql .= ');';

        $stmt = $this->db->prepare($sql);

        $this->db->exec($stmt, $values);

        return $this->db->getDb()->lastInsertId();
    }

    /**
     * Perform update query with prepared statement
     *
     * @param EntityInterface $entity
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function update(EntityInterface $entity)
    {
        $sql = "UPDATE $this->tableName SET ";
        $reflector = new ReflectionClass($this->className);

        // values of each field to perform the insert after the loop
        $values = [];

        foreach ($properties = $reflector->getProperties() as $index => $propertie) {
            $config = $this->getField($propertie->getName());

            if ($config['config']['type'] !== 'relation' ||
                (
                    $config['config']['type'] === 'relation' &&
                    (
                        $config['config']['relation_type'] === 'manyToOne' ||
                        $config['config']['relation_type'] === 'oneToOne'

                    )
                )
            ) {
                $getter = 'get' . $propertie->getName();
                $value = $entity->$getter();

                if ($value !== null) {
                    if ($value instanceof DateTimeInterface) {
                        $value = $value->format('Y-m-d');
                    } elseif ($value instanceof EntityInterface) {
                        $value = $value->getId();
                    } elseif (is_bool($value)) {
                        $value = $value ? 1 : 0;
                    } elseif ($config['config']['type'] === 'array') {
                        $value = serialize($value);
                    }
                    $values[$config['db_name']] = $value;
                }
            }
        }

        $sql .= implode(
            ', ',
            array_map(
                function ($elem) {
                    return $elem . ' = :' . $elem;
                },
                array_keys($values)
            )
        );

        $sql .= " WHERE $this->tableName.id = :id";

        $stmt = $this->db->prepare($sql);
        return $this->db->exec($stmt, $values);
    }

    /**
     * Delete a record from database
     *
     * @param EntityInterface $entity
     *
     * @return false|int
     * @throws Exception
     */
    public function delete(EntityInterface $entity)
    {
        $id = $entity->getId();
        $sql = "DELETE FROM $this->tableName WHERE $this->tableName.id = ?";

        $stmt = $this->db->prepare($sql);
        return $this->db->exec($stmt, [$id]);
    }

    /**
     * find config by dbName or entity property name
     * Return array form the config file or null if field does not exist
     * @param string $name
     * @return array|null
     */
    private function getField(string $name): ?array
    {
        $config = null;

        if (isset($this->fields[$name])) {
            $config = [
                'db_name' => $name,
                'config' => $this->fields[$name]
            ];
        } else {
            foreach ($this->fields as $dbName => $field) {
                if ($field['name'] === $name) {
                    $config = [
                        'db_name' => $dbName,
                        'config' => $field
                    ];
                }
            }
        }

        return $config;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    public function createQueryBuilder(string $alias = null)
    {
        if (!$alias) {
            $alias = strtolower(substr($this->tableName, 0, 1));
        }
        return new QueryBuilder($this->tableName, $alias);
    }

    /**
     * Create a new Entity object and set values
     * @param $result
     * @param array $parsedEntityRelation array of already hydrated objects to prevent infinite looping
     *                                      over bidirectional relations
     * @return EntityInterface object
     * @throws OrmNotFoundException
     */
    public function hydrateObject($result, array $parsedEntityRelation = [])
    {
        $entity = new $this->className();

        if (!is_array($result) && is_object($result)) {
            $result = get_object_vars($result);
        }

        foreach ($this->getFields() as $field => $properties) {
            try {
                $setter = 'set' . ucfirst($properties['name']);

                // if $result[$field] !isset value should be id to fetch for relation
                if (array_key_exists($field, $result)) {
                    $value = $result[$field];
                } else {
                    $value = $result['id'];
                }

                if ($properties['type'] === 'date' || $properties['type'] === 'datetime') {
                    if ($value) {
                        $value = new DateTimeImmutable($value);
                    }
                }

                if ($properties['type'] === 'array') {
                    if ($value) {
                        $value = unserialize($value);
                    }
                }

                if ($properties['type'] === 'relation' &&
                    !array_key_exists($properties['name'], $parsedEntityRelation)
                ) {
                    $repoStr = str_replace('Entity', 'Repository', $properties['class']) . 'Repository';
                    $relationRepo = Container::getInstance()->get($repoStr);

                    switch ($properties['relation_type']) {
                        case 'oneToMany':
                            $value = $relationRepo->findBy(
                                [$properties['mapped_by']['name'] => $result['id']],
                                [$properties['mapped_by']['name'] => $entity]
                            );
                            break;
                        case 'oneToOne':
                        case 'manyToOne':
                            $value = $relationRepo->find($value);
                            break;
                        default:
                            throw new OrmNotFoundException(
                                sprintf(
                                    'Relation type : "%s" not known, check entity config file.',
                                    $field['relation_type'] ?: null
                                )
                            );
                    }
                } elseif (array_key_exists($properties['name'], $parsedEntityRelation)) {
                    $value = $parsedEntityRelation[$properties['name']];
                }

                $entity->$setter($value);
            } catch (Exception $e) {
                throw new OrmNotFoundException(
                    sprintf(
                        'Setter not found for "%s", error : "%s"',
                        $this->className,
                        $e->getMessage()
                    )
                );
            }
        }

        return $entity;
    }

    /**
     * Loop over results, hydrate objects and return array of entities
     * @param $results
     * @param array $parsedEntityRelation
     * @return array
     * @throws OrmNotFoundException
     */
    public function hydrateMany($results, array $parsedEntityRelation = []): array
    {
        $entities = [];
        foreach ($results as $result) {
            $entities[] = $this->hydrateObject($result, $parsedEntityRelation);
        }
        return $entities;
    }
}
