<?php

namespace Framework\Orm;

interface EntityInterface
{
    /**
     * Return id of the entity
     * @return string|int|null
     */
    public function getId(): ?string;
}
