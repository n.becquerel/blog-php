<?php

namespace Framework\Orm;

use App\Entity\Post;
use App\Repository\PostRepository;
use Framework\Parser\YAMLParser;

class RepositoryFactory
{
    /**
     * Instantiate the corresponding repository for given entity className
     * @param $className
     * @return mixed
     */
    public static function createRepository($className)
    {
        $parser = YAMLParser::getInstance();
        $class = explode('\\', $className);
        $metadata = $parser->withPath('src/Entity/config')->getContent(end($class));
        $repository = $metadata['repository'];

        return new $repository($metadata);
    }
}
