<?php

namespace Framework\Orm;

/**
 * Is used to create reading queries
 * Class QueryBuilder
 * @package Framework\Orm
 */
class QueryBuilder
{
    /**
     * @var string
     */
    private $query;

    /**
     * @var string
     */
    private $select = '';

    /**
     * @var string
     */
    private $from = '';

    /**
     * @var string
     */
    private $where = '';

    /**
     * @var string
     */
    private $orderBy = '';

    /**
     * @var string
     */
    private $join = '';

    /**
     * @var string
     */
    private $groupBy = '';

    /**
     * @var string
     */
    private $limit = '';

    /**
     * @var string
     */
    private $alias = '';

    /**
     * @var array
     */
    private $parameters = [];

    /**
     * @var Database
     */
    private $db;

    public function __construct(string $tableName, string $alias)
    {
        $this->from = "FROM `$tableName` as `$alias`";

        $this->alias = $alias;

        $this->db = Database::getInstance();
    }

    /**
     * Take all parts of the query and store the completed query in $this->query
     * @return QueryBuilder
     */
    public function getQuery(): QueryBuilder
    {
        if (empty($this->select)) {
            $this->select = 'SELECT *';
        }

        $query = "$this->select $this->from ";

        if (!empty($this->join)) {
            $query .= $this->join . ' ';
        }

        if (!empty($this->where)) {
            $query .= $this->where . ' ';
        }

        if (!empty($this->orderBy)) {
            $query .= $this->orderBy . ' ';
        }

        if (!empty($this->groupBy)) {
            $query .= $this->groupBy . ' ';
        }

        if (!empty($this->limit)) {
            $query .= $this->limit . ' ';
        }

        $this->query = $query;

        return $this;
    }

    /**
     * Execute and fetchAll the query to get all results
     * @return array
     */
    public function getResult()
    {
        $stmt = $this->db->query($this->query, $this->parameters);

        return $this->db->fetchAll($stmt);
    }

    /**
     * Execute and fetch the query to get a single result
     * @return mixed|null
     */
    public function getSingleResult()
    {
        $stmt = $this->db->query($this->query, $this->parameters);

        return $this->db->fetch($stmt);
    }

    /**
     * @param string $select
     * @return QueryBuilder
     */
    public function select(string $select): QueryBuilder
    {
        $this->select = "SELECT $select";

        return $this;
    }

    /**
     * @param string $condition
     * @return QueryBuilder
     */
    public function where(string $condition): QueryBuilder
    {
        $this->where = "WHERE $condition";

        return $this;
    }

    /**
     * @param string $condition
     * @return QueryBuilder
     */
    public function orWhere(string $condition): QueryBuilder
    {
        $this->where .= " OR $condition";

        return $this;
    }

    /**
     * @param string $condition
     * @return QueryBuilder
     */
    public function andWhere(string $condition): QueryBuilder
    {
        $this->where .= " AND $condition";

        return $this;
    }

    /**
     * @param string $field
     * @param string $order
     * @return QueryBuilder
     */
    public function orderBy(string $field, string $order = 'ASC'): QueryBuilder
    {
        $this->orderBy = "ORDER BY $field $order";

        return $this;
    }

    /**
     * @param string $field
     * @param string $order
     * @return QueryBuilder
     */
    public function addOrderBy(string $field, string $order = 'ASC'): QueryBuilder
    {
        $this->orderBy .= ", $field $order";

        return $this;
    }

    /**
     * @param string $table
     * @param string $condition
     * @return QueryBuilder
     */
    public function join(string $table, string $alias, string $condition): self
    {
        $this->join .= 'JOIN ' . $table . ' ' . $alias . ' on ' . $condition;

        return $this;
    }

    /**
     * @param array $parameters
     * @return QueryBuilder
     */
    public function setParameters(array $parameters): QueryBuilder
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }
}
