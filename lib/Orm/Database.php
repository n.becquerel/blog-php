<?php

namespace Framework\Orm;

use Exception;
use Framework\Parser\YAMLParser;
use InvalidArgumentException;
use PDO;
use PDOStatement;

class Database
{
    /**
     * Default mode for fetching db records
     */
    private const DEFAULT_FETCH_MODE = PDO::FETCH_OBJ;

    /**
     * @var Database
     */
    public static $instance;

    /**
     * @var array
     */
    private $config = [];

    /**
     * @var PDO
     */
    private $db;

    public function __construct(YAMLParser $parser)
    {
        $this->config = $parser->withPath('config')->getContent('parameters');
        if (!$this->db) {
            $this->db = $this->connect();
        }
    }

    public function __destruct()
    {
        $this->db = null;
    }

    /**
     * Connect to database according to parameters in parameters.yaml and return the PDO object
     * @return PDO
     * @throws InvalidArgumentException if config could not be read from
     */
    private function connect()
    {
        if (!isset($this->config['database']['driver']) ||
            !isset($this->config['database']['host']) ||
            !isset($this->config['database']['db_user']) ||
            !isset($this->config['database']['db_name'])
        ) {
            throw new InvalidArgumentException('Invalid configuration in "config/parameters.yaml".');
        }

        return new PDO(
            $this->getDsn(),
            $this->config['database']['db_user'],
            $this->config['database']['db_password'],
            [PDO::MYSQL_ATTR_INIT_COMMAND => 'set names utf8']
        );
    }

    /**
     * Perform query in database
     * @param $query
     * @param array $params
     * @return PDOStatement
     */
    public function query($query, array $params = []): PDOStatement
    {
        $stmt = $this->db->prepare($query);

        $stmt->execute($params);

        return $stmt;
    }

    /**
     * @param PDOStatement $stmt
     * @param int|null $mode
     * @return mixed|null
     */
    public function fetch(PDOStatement $stmt, int $mode = null)
    {
        return $stmt->fetch($mode ?: self::DEFAULT_FETCH_MODE);
    }

    /**
     * @param PDOStatement $stmt
     * @param int|null $mode
     * @return array
     */
    public function fetchAll(PDOStatement $stmt, int $mode = null)
    {
        return $stmt->fetchAll($mode ?: self::DEFAULT_FETCH_MODE);
    }

    /**
     * @param string $query sql query
     *
     * @return bool|PDOStatement
     */
    public function prepare(string $query)
    {
        return $this->db->prepare($query);
    }

    /**
     * @param PDOStatement $stmt
     * @param array $parameters array of values for prepared statement
     * @return mixed
     * @throws Exception
     */
    public function exec($stmt, array $parameters)
    {
//        dump($parameters);
        $result = $stmt->execute($parameters);
//        dd($stmt->errorInfo());
        if ($result === false) {
            throw new Exception($stmt->errorCode() . ' : ' . implode('; ', $stmt->errorInfo()));
        }

        return $result;
    }

    private function getDsn()
    {
        $dsn = $this->config['database']['driver'] . ':host=' . $this->config['database']['host'];

        if (isset($this->config['database']['port'])) {
            $dsn .= ':' . $this->config['database']['port'];
        }

        $dsn .= ';dbname=' . $this->config['database']['db_name'];

        return $dsn;
    }

    /**
     * @return mixed
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new Database(new YAMLParser());
        }

        return self::$instance;
    }

    /**
     * @return PDO
     */
    public function getDb(): PDO
    {
        return $this->db;
    }
}
