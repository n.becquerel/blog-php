<?php

namespace App\Security;

use App\Repository\UserRepository;
use Framework\Security\AuthenticatorInterface;
use Framework\Security\UserInterface;

class AppAuthenticator implements AuthenticatorInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * AppAuthenticator constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getCredentials(array $bodyRequest): ?array
    {
        if (!isset($bodyRequest['_email']) &&
            !isset($bodyRequest['_password'])
        ) {
            return null;
        }

        return [
            'email' => $bodyRequest['_email'],
            'password' => $bodyRequest['_password']
        ];
    }

    public function getUser(array $credentials): ?UserInterface
    {
        try {
            $result = $this->userRepository->findBy(['email' => $credentials['email']]);

            if (count($result) === 1) {
                return $result[0];
            }

            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function checkCredentials(array $credentials, UserInterface $user): bool
    {
        return password_verify($credentials['password'], $user->getPassword());
    }
}
