<?php

namespace App\Security;

use App\Repository\UserRepository;
use Framework\Security\Providers\UserProviderInterface;
use Framework\Security\UserInterface;
use Framework\Session\SessionManager;

class SessionUserProvider implements UserProviderInterface
{
    /**
     * @var SessionManager
     */
    private $sessionManager;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * SessionUserProvider constructor.
     * @param SessionManager $sessionManager
     * @param UserRepository $userRepository
     */
    public function __construct(SessionManager $sessionManager, UserRepository $userRepository)
    {
        $this->sessionManager = $sessionManager;
        $this->userRepository = $userRepository;
    }

    /**
     * @inheritDoc
     * @return UserInterface|null
     */
    public function getUser(): ?UserInterface
    {
        $user = null;

        if ($this->sessionManager->has('user')) {
            $serializedUser = $this->sessionManager->get('user');

            /** @var UserInterface $user */
            $user = unserialize($serializedUser);

            $user = $this->getUserByEmail($user->getEmail());

            if ($user) {
                $user->eraseCredentials();
            }
        }

        return $user;
    }

    private function getUserByEmail($email): ?UserInterface
    {
        $users = $this->userRepository->findBy(['email' => $email]);

        if (count($users) !== 1) {
            return null;
        }

        return $users[0];
    }
}
