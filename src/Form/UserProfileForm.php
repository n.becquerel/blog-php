<?php

namespace App\Form;

use App\Entity\User;
use Framework\Form\Form;

class UserProfileForm extends Form
{
    public function buildForm(array $options = []): void
    {
        $this
            ->add(
                'username',
                'text',
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Nom d\'utilisateur'
                ]
            )
            ->add(
                'firstname',
                'text',
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Prénom'
                ]
            )
            ->add(
                'lastname',
                'text',
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Nom'
                ]
            )
            ->add(
                'description',
                'textarea',
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Description'
                ]
            )
            ->add(
                'quote',
                'text',
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Citation'
                ]
            );
    }

    public function getPrefix(): string
    {
        return 'user_profile';
    }

    public function getClassName(): string
    {
        return User::class;
    }
}
