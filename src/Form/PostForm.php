<?php

namespace App\Form;

use App\Entity\BlogPost;
use App\Entity\Post;
use App\Entity\PostCategory;
use App\Repository\PostCategoryRepository;
use Assert\Assertion;
use Framework\Form\Form;
use Framework\Form\FormInterface;
use Framework\Orm\EntityInterface;

class PostForm extends Form
{
    public function buildForm(array $options = []): void
    {
        $this
            ->add(
                'mainTitle',
                'text',
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Titre principal'
                ]
            )
            ->add(
                'shortDescription',
                'text',
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Description du post'
                ]
            )
            ->add(
                'content',
                'wysiwyg',
                [
                    'attr' => [
                        'data-plugin' => 'wysiwyg',
                        'class' => 'form-control'
                    ],
                    'label' => 'Contenu de l\'article',
                    'auto_escape' => false
                ]
            )
            ->add(
                'category',
                'entity',
                [
                    'class' => PostCategory::class,
                    'repository' => PostCategoryRepository::class,
                    //                'choices' => $options['categories'],
                    'placeholder' => 'Choisissez une catégorie',
                    'choice_label' => 'label',
                    'attr' => [
                        'class' => 'form-control',
                    ],
                    'label' => 'Categorie',
                    'constraints' => [
                        'notNull' => [
                            'message' => 'Veuillez choisir une catégorie.'
                        ]
                    ]
                ]
            );
    }

    public function getPrefix(): string
    {
        return 'post';
    }

    public function getClassName(): string
    {
        return BlogPost::class;
    }
}
