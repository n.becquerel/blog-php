<?php

namespace App\Form;

use App\Entity\User;
use Framework\Form\Form;

class UserForm extends Form
{
    public function buildForm(array $options = []): void
    {
        $this
            ->add('email', 'text', [
                'attr' => [
                    'class' => 'form-control'
                ],
                'label' => 'Adresse email',
                'constraints' => [
                    'notEmpty' => [
                        'message' => 'L\'adressse email est obligatoire.'
                    ],
                    'email' =>  [
                        'message' => 'L\'adresse mail n\'est pas valide.'
                    ]
                ]
            ])
            ->add('password', 'password', [
                'attr' => [
                    'class' => 'form-control'
                ],
                'label' => 'Mot de passe',
                'constraints' => [
                    'notEmpty' => [
                        'message' => 'Veuillez choisir un mot de passe'
                    ],
                    'betweenLength' => [
                        'minLength' => 4,
                        'maxLength' => 70,
                        'message' => 'Le mot de passe doit faire entre 4 et 70 caractères.'
                    ]
                ]
            ])
            ->add('username', 'text', [
                'attr' => [
                    'class' => 'form-control',
                ],
                'label' => 'Nom d\'utilisateur',
                'constraints' => [
                    'notEmpty' => [
                        'message' => 'Veuillez choisir un nom d\'utilisateur'
                    ],
                ]
            ])
        ;
    }

    public function getPrefix(): string
    {
        return 'account';
    }

    public function getClassName(): string
    {
        return User::class;
    }
}
