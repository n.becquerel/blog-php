<?php

namespace App\Form;

use App\Entity\ContactRequest;
use Framework\Form\Form;

class ContactForm extends Form
{
    public function buildForm(array $options = []): void
    {
        $this
            ->add(
                'lastname',
                'text',
                [
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Nom',
                    ],
                    'label' => 'Nom',
                ]
            )
            ->add(
                'firstname',
                'text',
                [
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Prénom'
                    ],
                    'label' => 'Prénom'
                ]
            )
            ->add(
                'email',
                'email',
                [
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Adresse email'
                    ],
                    'label' => 'Adresse email'
                ]
            )
            ->add(
                'message',
                'textarea',
                [
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Message'
                    ],
                    'label' => 'Message'
                ]
            );
    }

    public function getPrefix(): string
    {
        return 'contact';
    }

    public function getClassName(): string
    {
        return ContactRequest::class;
    }
}
