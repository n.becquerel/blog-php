<?php

namespace App\Form;

use App\Entity\PostComment;
use Framework\Form\Form;

class CommentForm extends Form
{
    public function buildForm(array $options = []): void
    {
        $this
            ->add(
                'body',
                'textarea',
                [
                    'attr' => [
                        'class' => 'form-control',
                    ],
                    'label' => 'Commentez cet article'
                ]
            );
    }

    public function getPrefix(): string
    {
        return 'comment';
    }

    public function getClassName(): string
    {
        return PostComment::class;
    }
}
