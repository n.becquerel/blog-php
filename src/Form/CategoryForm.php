<?php

namespace App\Form;

use App\Entity\PostCategory;
use Framework\Form\Form;

class CategoryForm extends Form
{
    public function buildForm(array $options = []): void
    {
        $this
            ->add(
                'label',
                'text',
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Libellé',
                    'constraints' => [
                        'notNull' => [
                            'message' => 'Vous devez saisir le nom de la catégorie.'
                        ]
                    ]
                ]
            );
    }

    public function getPrefix(): string
    {
        return 'category';
    }

    public function getClassName(): string
    {
        return PostCategory::class;
    }
}
