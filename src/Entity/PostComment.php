<?php

namespace App\Entity;

use Framework\Orm\EntityInterface;

class PostComment implements EntityInterface
{
    /**
     * @var string|null
     */
    private $id;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $body;

    /**
     * @var \DateTimeInterface
     */
    private $createdAt;

    /**
     * @var bool
     */
    private $visible;

    /**
     * @var bool
     */
    private $moderated;

    /**
     * @var \DateTimeInterface
     */
    private $updatedAt;

    /**
     * @var BlogPost
     */
    private $post;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param  string|null $id
     * @return PostComment
     */
    public function setId(?string $id): PostComment
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param  User $user
     * @return PostComment
     */
    public function setUser(User $user): PostComment
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param  \DateTimeInterface $createdAt
     * @return PostComment
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): PostComment
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    /**
     * @param  bool $visible
     * @return PostComment
     */
    public function setVisible(bool $visible): PostComment
    {
        $this->visible = $visible;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getModerated(): ?bool
    {
        return $this->moderated;
    }

    /**
     * @param  bool $moderated
     * @return PostComment
     */
    public function setModerated(bool $moderated): PostComment
    {
        $this->moderated = $moderated;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param  \DateTimeInterface|null $updatedAt
     * @return PostComment
     */
    public function setUpdatedAt(?\DateTimeInterface $updatedAt): PostComment
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return BlogPost
     */
    public function getPost(): BlogPost
    {
        return $this->post;
    }

    /**
     * @param  BlogPost $post
     * @return PostComment
     */
    public function setPost(BlogPost $post): PostComment
    {
        $this->post = $post;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @param  string $body
     * @return PostComment
     */
    public function setBody(string $body): PostComment
    {
        $this->body = $body;
        return $this;
    }
}
