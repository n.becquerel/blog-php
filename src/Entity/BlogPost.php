<?php

namespace App\Entity;

use DateTimeInterface;
use Framework\Orm\EntityInterface;

class BlogPost implements EntityInterface
{
    /**
     * @var string|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $shortDescription;

    /**
     * @var DateTimeInterface|null
     */
    private $createdAt;

    /**
     * @var string|null
     */
    private $mainTitle;

    /**
     * @var string
     */
    private $content;

    /**
     * @var DateTimeInterface|null
     */
    private $updatedAt;

    /**
     * @var PostCategory|null
     */
    private $category;

    /**
     * @var PostComment[]|null
     */
    private $comments;

    /**
     * @var User|null
     */
    private $user;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param  string|null $id
     * @return BlogPost
     */
    public function setId(?string $id): BlogPost
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    /**
     * @param  string|null $shortDescription
     * @return BlogPost
     */
    public function setShortDescription(?string $shortDescription): BlogPost
    {
        $this->shortDescription = $shortDescription;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param  DateTimeInterface|null $createdAt
     * @return BlogPost
     */
    public function setCreatedAt(?DateTimeInterface $createdAt): BlogPost
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMainTitle(): ?string
    {
        return $this->mainTitle;
    }

    /**
     * @param  string|null $mainTitle
     * @return BlogPost
     */
    public function setMainTitle(?string $mainTitle): BlogPost
    {
        $this->mainTitle = $mainTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param  null|string $content
     * @return BlogPost
     */
    public function setContent(?string $content): BlogPost
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param  DateTimeInterface|null $updatedAt
     * @return BlogPost
     */
    public function setUpdatedAt(?DateTimeInterface $updatedAt): BlogPost
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return PostCategory|null
     */
    public function getCategory(): ?PostCategory
    {
        return $this->category;
    }

    /**
     * @param  PostCategory|null $category
     * @return BlogPost
     */
    public function setCategory(?PostCategory $category): BlogPost
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return PostComment[]|null
     */
    public function getComments(): ?array
    {
        return $this->comments;
    }

    /**
     * @param  PostComment[]|null $comments
     * @return BlogPost
     */
    public function setComments(?array $comments): BlogPost
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param  User|null $user
     * @return BlogPost
     */
    public function setUser(?User $user): BlogPost
    {
        $this->user = $user;
        return $this;
    }
}
