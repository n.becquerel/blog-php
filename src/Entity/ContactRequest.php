<?php

namespace App\Entity;

class ContactRequest
{
    /**
     * @var string
     */
    private $lastname;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $message;

    /**
     * @return string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param  string $lastname
     * @return ContactRequest
     */
    public function setLastname(string $lastname): ContactRequest
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param  string $firstname
     * @return ContactRequest
     */
    public function setFirstname(string $firstname): ContactRequest
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param  string $email
     * @return ContactRequest
     */
    public function setEmail(string $email): ContactRequest
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param  string $message
     * @return ContactRequest
     */
    public function setMessage(string $message): ContactRequest
    {
        $this->message = $message;
        return $this;
    }
}
