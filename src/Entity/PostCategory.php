<?php

namespace App\Entity;

use Framework\Orm\EntityInterface;

class PostCategory implements EntityInterface
{
    /**
     * @var string|null
     */
    private $id;

    /**
     * @var string
     */
    private $label;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param  string|null $id
     * @return PostCategory
     */
    public function setId(?string $id): PostCategory
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param  string $label
     * @return PostCategory
     */
    public function setLabel(?string $label): PostCategory
    {
        $this->label = $label;
        return $this;
    }
}
