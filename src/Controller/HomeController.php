<?php

namespace App\Controller;

use App\Entity\ContactRequest;
use App\Form\ContactForm;
use App\Repository\BlogPostRepository;
use App\Repository\PostCategoryRepository;
use App\Repository\PostRepository;
use App\Services\AppMailer;
use Framework\Controller\BaseController;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Environment;

class HomeController extends BaseController
{
    /**
     * @var BlogPostRepository
     */
    private $postRepository;

    /**
     * @var AppMailer
     */
    private $mailer;

    public function __construct(Environment $twig, BlogPostRepository $postRepository, AppMailer $mailer)
    {
        parent::__construct($twig);
        $this->postRepository = $postRepository;
        $this->mailer = $mailer;
    }

    public function index(ServerRequestInterface $request)
    {
        $options = [];
        $category = null;

        $queryParams = $request->getQueryParams();

        if (isset($queryParams['category'])) {
            $options['category'] = $queryParams['category'];
            $category = $this->container->get(PostCategoryRepository::class)->find($options['category']);
        }

        if (isset($queryParams['flashe'])) {
            $this->addFlash('danger', 'Vous n\'êtes pas connecté');
            $this->addFlash('danger', 'test');
            $this->addFlash('success', 'Bravo');
        }

        $posts = $this->postRepository->getList($options);

        return $this->render('home/index.html.twig', [
            'posts' => $posts,
            'category' => $category
        ]);
    }

    /**
     * GET - toggle contact form view
     *
     * @param  ServerRequestInterface $request
     * @return \Framework\Http\Message|\Framework\Http\Response|\Psr\Http\Message\MessageInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function contactMeAction(ServerRequestInterface $request)
    {
        $contactRequest = new ContactRequest();

        $form = $this->createForm(ContactForm::class, $contactRequest);

        $form->handleRequest($request);

        if ($request->getMethod() === 'POST' && $form->isValid()) {
            $success = $this->mailer->sendMail([], 'test', 'contact/mail-template.html.twig', [
                'contactRequest' => $contactRequest
            ]);

            if ($success) {
                $this->addFlash('success', 'Votre demande à bien été envoyée.');
                return $this->redirectToRoute('home');
            } else {
                $this->addFlash(
                    'danger',
                    'Une erreur est survenue, veuillez réessayer ou contacter un administrateur',
                    false
                );
            }
        }

        return $this->render(
            'contact/form.html.twig',
            [
            'form' => $form->createView()
            ]
        );
    }
}
