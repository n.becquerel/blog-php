<?php

namespace App\Controller;

use App\Entity\BlogPost;
use App\Entity\PostComment;
use App\Form\CommentForm;
use App\Form\PostForm;
use App\Repository\BlogPostRepository;
use App\Repository\PostCategoryRepository;
use App\Repository\PostCommentRepository;
use App\Repository\PostParagraphRepository;
use App\Repository\UserRepository;
use Framework\Controller\BaseController;
use Framework\Http\Response;
use Framework\Http\ServerRequest;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Environment;

class PostController extends BaseController
{
    /**
     * @var BlogPostRepository
     */
    private $postRepository;

    /**
     * @var PostParagraphRepository
     */
    private $paragraphRepository;

    /**
     * @var PostCommentRepository
     */
    private $commentRepository;

    public function __construct(
        Environment $twig,
        BlogPostRepository $postRepository,
        PostParagraphRepository $paragraphRepository,
        PostCommentRepository $commentRepository
    ) {
        parent::__construct($twig);
        $this->postRepository = $postRepository;
        $this->paragraphRepository = $paragraphRepository;
        $this->commentRepository = $commentRepository;
    }

    /**
     * Show a post entity
     *
     * @param  $id
     * @param  ServerRequest $request
     * @return \Framework\Http\Message|\Framework\Http\Response|\Psr\Http\Message\MessageInterface
     * @throws \Framework\Orm\OrmNotFoundException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function showAction($id, ServerRequest $request)
    {
        $post = $this->postRepository->find($id);

        if (!$post) {
            return new Response(404);
        }

        $comments = $this->commentRepository->findPostCommentaires(
            $post->getId(),
            $this->security->isGranted('ROLE_ADMIN')
        );

        $comment = new PostComment();
        $commentForm = $this->createForm(CommentForm::class, $comment);

        $commentForm->handleRequest($request);

        if ($request->getMethod() === 'POST' && $commentForm->isValid()) {
            if (!$this->security->isGranted('ROLE_USER')) {
                $this->addFlash('danger', 'Vous devez être connecté pour poster un commentaire.');
                return $this->redirectToRoute('post_show', ['id' => $post->getId()]);
            }

            $comment->setUser($this->container->get(UserRepository::class)->find(1))
                ->setCreatedAt(new \DateTime())
                ->setPost($post);
            $this->commentRepository->insert($comment);

            $this->addFlash('success', 'Commentaire crée, il sera soumis à modération.');
            return $this->redirectToRoute('post_show', ['id' => $post->getId()]);
        }

        return $this->render(
            'post/show.html.twig',
            [
            'post' => $post,
            'comments' => $comments,
            'commentForm' => $commentForm->createView()
            ]
        );
    }

    /**
     * Edit a post entity
     *
     * @param  ServerRequestInterface $request
     * @param  $id
     * @return \Framework\Http\Message|\Framework\Http\Response|\Psr\Http\Message\MessageInterface
     * @throws \Framework\Orm\OrmNotFoundException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function editAction(ServerRequestInterface $request, $id)
    {
        $user = $this->security->getUser();

        /** @var BlogPost $post */
        $post = $this->postRepository->find($id);

        if (!$post) {
            return new Response(404, null);
        }

        if (!$this->security->isGranted('ROLE_ADMIN') || $user->getId() !== $post->getUser()->getId()) {
            $this->addFlash('danger', 'Vous n\'avez pas les droits pour accéder à cette page.');
            return $this->redirectToRoute('post_show', ['id' => $post->getId()]);
        }

        $form = $this->createForm(PostForm::class, $post);

        $form->handleRequest($request);

        if ($request->getMethod() === 'POST' && $form->isValid()) {
            $post->setUpdatedAt(new \DateTime());
            $this->postRepository->update($post);

            $this->addFlash('success', 'Post modifié.', false);
        }

        return $this->render(
            'post/edit.html.twig',
            [
            'post' => $post,
            'form' => $form->createView()
            ]
        );
    }

    /**
     * @param  ServerRequestInterface $request
     * @return \Framework\Http\Message|Response|\Psr\Http\Message\MessageInterface
     */
    public function newAction(ServerRequestInterface $request)
    {
        if (!$this->security->isGranted('ROLE_USER')) {
            $this->addFlash('danger', 'Vous devez être connecté pour ajouter un post.');
            return $this->redirectToRoute('home');
        }

        $blogPost = new BlogPost();

        $categories = $this->container->get(PostCategoryRepository::class)->findAll();

        $form = $this->createForm(PostForm::class, $blogPost, ['categories' => $categories]);

        $form->handleRequest($request);

        if ($request->getMethod() === 'POST' && $form->isValid()) {
            $blogPost->setCreatedAt(new \DateTime())
                ->setUser($this->container->get(UserRepository::class)->find(1));

            $this->postRepository->insert($blogPost);
            $this->addFlash('success', 'Post crée.', false);
        }

        return $this->render(
            'post/edit.html.twig',
            [
            'form' => $form->createView()
            ]
        );
    }
}
