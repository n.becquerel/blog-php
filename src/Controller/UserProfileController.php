<?php

namespace App\Controller;

use App\Form\UserProfileForm;
use App\Repository\UserRepository;
use Framework\Controller\BaseController;
use Framework\Http\Response;
use Framework\Http\ServerRequest;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Environment;

class UserProfileController extends BaseController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserProfileController constructor.
     *
     * @param UserRepository $userRepository
     * @param Environment    $twig
     */
    public function __construct(UserRepository $userRepository, Environment $twig)
    {
        $this->userRepository = $userRepository;
        parent::__construct($twig);
    }

    public function showAction()
    {
        if (!$this->security->isGranted('ROLE_USER')) {
            $this->addFlash('danger', 'Vous devez être connecté pour accéder à cette ressource.');
            return $this->redirectToRoute('home');
        }

        $user = $this->userRepository->find($this->security->getUser()->getId());

        if (!$user) {
            return new Response(404);
        }

        return $this->render('user-profile/show.html.twig', ['user' => $user]);
    }

    public function editAction(ServerRequest $request)
    {
        if (!$this->security->isGranted('ROLE_USER')) {
            $this->addFlash('danger', 'Vous devez être connecté pour accéder à cette ressource.');
            return $this->redirectToRoute('home');
        }

        $user = $this->userRepository->find($this->security->getUser()->getId());

        if (!$user) {
            return new Response(404);
        }

        $form = $this->createForm(UserProfileForm::class, $user);

        $form->handleRequest($request);

        if ($request->getMethod() === 'POST' && $form->isValid()) {
            $this->userRepository->update($user);

            $this->addFlash('success', 'Profil modifié.');

            return $this->redirectToRoute('user_profile_show');
        }

        return $this->render('user-profile/edit.html.twig', ['user' => $user, 'form' => $form->createView()]);
    }
}
