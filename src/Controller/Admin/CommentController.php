<?php

namespace App\Controller\Admin;

use App\Entity\PostComment;
use App\Repository\PostCategoryRepository;
use App\Repository\PostCommentRepository;
use Framework\Controller\BaseController;
use Framework\Http\Response;
use Twig\Environment;

class CommentController extends BaseController
{
    /**
     * @var PostCommentRepository
     */
    private $commentRepository;

    /**
     * CategoryController constructor.
     *
     * @param PostCommentRepository $commentRepository
     * @param Environment           $twig
     */
    public function __construct(PostCommentRepository $commentRepository, Environment $twig)
    {
        $this->commentRepository = $commentRepository;
        parent::__construct($twig);
    }

    public function valideCommentAction($commentId)
    {
        $this->requireAdmin();

        /** @var PostComment $comment */
        $comment = $this->commentRepository->find($commentId);

        if (!$comment) {
            return new Response(404);
        }

        $comment->setModerated(true)
            ->setVisible(true)
            ->setUpdatedAt(new \DateTime());

        $this->commentRepository->update($comment);

        $this->addFlash('success', 'Commentaire validé et modéré.');

        return $this->redirectToRoute('post_show', ['id' => $comment->getPost()->getId()]);
    }

    public function hideCommentAction($commentId)
    {
        $this->requireAdmin();

        /** @var PostComment $comment */
        $comment = $this->commentRepository->find($commentId);

        if (!$comment) {
            return new Response(404);
        }

        $comment->setVisible(0)
            ->setUpdatedAt(new \DateTime());

        $this->commentRepository->update($comment);

        $this->addFlash('success', 'Commentaire caché aux utilisateurs.');

        return $this->redirectToRoute('post_show', ['id' => $comment->getPost()->getId()]);
    }

    public function showCommentAction($commentId)
    {
        $this->requireAdmin();

        /** @var PostComment $comment */
        $comment = $this->commentRepository->find($commentId);

        if (!$comment) {
            return new Response(404);
        }

        $comment->setVisible(1)
            ->setUpdatedAt(new \DateTime());

        $this->commentRepository->update($comment);

        $this->addFlash('success', 'Commentaire visible pour tous les utilisateurs.');

        return $this->redirectToRoute('post_show', ['id' => $comment->getPost()->getId()]);
    }

    public function deleteCommentAction($commentId)
    {
        $this->requireAdmin();

        /** @var PostComment $comment */
        $comment = $this->commentRepository->find($commentId);

        if (!$comment) {
            return new Response(404);
        }

        $this->commentRepository->delete($comment);

        $this->addFlash('success', 'Commentaire supprimé.');

        return $this->redirectToRoute('post_show', ['id' => $comment->getPost()->getId()]);
    }
}
