<?php

namespace App\Controller\Admin;

use App\Entity\PostCategory;
use App\Form\CategoryForm;
use App\Repository\BlogPostRepository;
use App\Repository\PostCategoryRepository;
use Framework\Controller\BaseController;
use Framework\Http\Response;
use Framework\Http\ServerRequest;
use Twig\Environment;

class CategoryController extends BaseController
{
    /**
     * @var PostCategoryRepository
     */
    private $categoryRepository;

    /**
     * @var BlogPostRepository
     */
    private $postRepository;

    /**
     * CategoryController constructor.
     *
     * @param PostCategoryRepository $categoryRepository
     * @param BlogPostRepository $postRepository
     * @param Environment $twig
     */
    public function __construct(
        PostCategoryRepository $categoryRepository,
        BlogPostRepository $postRepository,
        Environment $twig
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
        parent::__construct($twig);
    }

    public function listAction()
    {
        $this->requireAdmin();

        $categories = $this->categoryRepository->findAll();

        return $this->render('admin/category/list.html.twig', ['categories' => $categories]);
    }

    public function addAction(ServerRequest $request)
    {
        $this->requireAdmin();

        $category = new PostCategory();

        $form = $this->createForm(CategoryForm::class, $category);

        $form->handleRequest($request);

        if ($request->getMethod() === 'POST' && $form->isValid()) {
            $this->categoryRepository->insert($category);
            $this->addFlash('success', 'Catégorie ajoutée.');

            return $this->redirectToRoute('admin_category_list');
        }

        return $this->render('admin/category/form.html.twig', ['form' => $form->createView(), 'category' => $category]);
    }

    public function editAction(ServerRequest $request, $id)
    {
        $this->requireAdmin();

        $category = $this->categoryRepository->find($id);

        if (!$category) {
            return new Response(404);
        }

        $form = $this->createForm(CategoryForm::class, $category);

        $form->handleRequest($request);

        if ($request->getMethod() === 'POST' && $form->isValid()) {
            $this->categoryRepository->update($category);
            $this->addFlash('success', 'Catégorie modifiée.');

            return $this->redirectToRoute('admin_category_list');
        }

        return $this->render('admin/category/form.html.twig', ['form' => $form->createView(), 'category' => $category]);
    }

    public function deleteAction($id)
    {
        $this->requireAdmin();

        $category = $this->categoryRepository->find($id);

        if (!$category) {
            return new Response(404);
        }

        $posts = $this->postRepository->findBy(['category' => $category->getId()]);

        if (empty($posts)) {
            $this->categoryRepository->delete($category);
            $this->addFlash('success', 'Catégorie supprimée.');
        } else {
            $this->addFlash(
                'danger',
                'Impossible de supprimer cette catégorie, car des post lui sont liés.'
            );
        }

        return $this->redirectToRoute('admin_category_list');
    }
}
