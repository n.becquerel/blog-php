<?php

namespace App\Controller\Admin;

use App\Entity\BlogPost;
use App\Repository\BlogPostRepository;
use Framework\Controller\BaseController;
use Framework\Http\Response;
use Twig\Environment;

class PostController extends BaseController
{
    /**
     * @var BlogPostRepository
     */
    private $postRepository;

    /**
     * PostController constructor.
     *
     * @param BlogPostRepository $postRepository
     * @param Environment        $twig
     */
    public function __construct(BlogPostRepository $postRepository, Environment $twig)
    {
        $this->postRepository = $postRepository;
        parent::__construct($twig);
    }

    public function listAction()
    {
        $this->requireAdmin();

        $posts = $this->postRepository->getList();

        return $this->render('admin/post/list.html.twig', ['posts' => $posts]);
    }

    public function deleteAction($id)
    {
        $this->requireAdmin();

        /** @var BlogPost $post */
        $post = $this->postRepository->find($id);

        if (!$post) {
            return new Response(404);
        }

        $this->postRepository->delete($post);
        $this->addFlash('success', 'Post supprimé.');

        return $this->redirectToRoute('admin_post_list');
    }
}
