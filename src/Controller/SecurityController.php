<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserForm;
use App\Repository\UserRepository;
use Framework\Controller\BaseController;
use Framework\Security\AuthenticatorInterface;
use Framework\Security\Encoder\UserPasswordEncoderInterface;
use Framework\Session\SessionManager;
use MongoDB\Driver\Session;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Environment;

class SecurityController extends BaseController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    /**
     * @var AuthenticatorInterface
     */
    private $authenticator;

    public function __construct(
        Environment $twig,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $userPasswordEncoder,
        AuthenticatorInterface $authenticator
    ) {
        $this->userRepository = $userRepository;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->authenticator = $authenticator;
        parent::__construct($twig);
    }

    public function login(ServerRequestInterface $request)
    {
        $errors = [];

        if ($request->getMethod() === 'POST') {
            $credentials = $this->authenticator->getCredentials($request->getParsedBody());

            $user = $this->authenticator->getUser($credentials);

            if (!$user) {
                $errors[] = ['message' => 'Aucun utilisateur ne corresponds aux identifiants saisis.'];
            } else {
                $success = $this->authenticator->checkCredentials($credentials, $user);

                if ($success) {
                    $this->container->get(SessionManager::class)->set('user', serialize($user));
//                    dump($this->container->get(SessionManager::class)->getSession());die;
                    $username = $user->getUserName();
                    $this->addFlash('success', "Bonjour $username !");
                    return $this->redirectToRoute('home');
                }

                $errors[] = ['message' => 'Identifiant ou mot de passe incorect.'];
            }
        }

        return $this->render('user-account/login.html.twig', ['errors' => $errors]);
    }

    public function logout(ServerRequestInterface $request)
    {
        if ($this->security->isAuthenticated()) {
            /** @var SessionManager $sessionManager */
            $sessionManager = $this->container->get(SessionManager::class);

            if ($sessionManager->has('user')) {
                $sessionManager->remove('user');
                $this->addFlash('success', 'Au revoir !');
                return $this->redirectToRoute('home');
            }
        }

        return $this->redirectToRoute('home');
    }

    public function signIn(ServerRequestInterface $request)
    {
        $user = new User();

        $form = $this->createForm(UserForm::class, $user);

        $form->handleRequest($request);

        if ($request->getMethod() === 'POST' && $form->isValid()) {
            $user = $this->userPasswordEncoder->encodeUserPassword($user, $user->getPassword());
            $user->setCreatedAt(new \DateTime());

            $this->userRepository->insert($user);

            $this->addFlash('success', 'Votre compte a bien été crée !');
            return $this->redirectToRoute('login');
        }

        return $this->render('user-account/sign-in.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
