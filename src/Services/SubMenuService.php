<?php

namespace App\Services;

use App\Repository\PostCategoryRepository;

class SubMenuService
{
    /**
     * @var PostCategoryRepository
     */
    private $categoryRepository;

    /**
     * SubMenuService constructor.
     * @param PostCategoryRepository $categoryRepository
     */
    public function __construct(PostCategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getCategories()
    {
        return $this->categoryRepository->findAll();
    }
}
