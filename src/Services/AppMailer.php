<?php

namespace App\Services;

use Exception;
use Framework\Container\Container;
use SendGrid;
use SendGrid\Mail\Mail;
use Twig\Environment;

class AppMailer
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var Mail
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * AppMailer constructor.
     *
     * @param Container $container
     * @param Environment $twig
     */
    public function __construct(Container $container, Environment $twig)
    {
        $this->container = $container;
        $this->twig = $twig;
        $this->mailer = new SendGrid($container->getParameter('sendgrid_api_key'));
    }


    /**
     * @param string|array $to
     * @param string $subject
     * @param array|string[] $from
     * @param string $template
     * @param string|array $contentData
     * @return bool
     * @throws SendGrid\Mail\TypeException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendMail(
        $to,
        string $subject,
        string $template,
        array $contentData = []
    ): bool {
        $mail = new Mail();

        if ($this->container->getParameter('env') === 'DEV') {
            $mail->addTo('contact@blog-php.nico-dev.com', 'Blog Php');
        } else {
            if (!empty($to)) {
                if (is_array($to)) {
                    foreach ($to as $email => $value) {
                        $mail->addTo($email, $value ?: null);
                    }
                } else {
                    $mail->addTo($to);
                }
            } else {
                $mail->addTo($this->container->getParameter('from_mail'), $this->container->getParameter('from_name'));
            }
        }

        $mail->setSubject($subject);

        $html = $this->twig->render($template, $contentData);

        $mail->addContent('text/html', $html);

        $mail->setFrom($this->container->getParameter('from_mail'), $this->container->getParameter('from_name'));

        try {
            $response = $this->mailer->send($mail);

            return $response->statusCode() === 204 || $response->statusCode() === 202;
        } catch (Exception $e) {
            return false;
        }
    }
}
