<?php

namespace App\Repository;

use Framework\Orm\Database;
use Framework\Orm\EntityRepository;
use Framework\Parser\YAMLParser;

class PostCategoryRepository extends EntityRepository
{
    private const CLASS_NAME = 'App\Entity\PostCategory';

    public function __construct(YAMLParser $parser, Database $db)
    {
        parent::__construct($parser, $db, self::CLASS_NAME);
    }
}
