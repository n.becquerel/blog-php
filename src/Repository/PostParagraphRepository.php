<?php

namespace App\Repository;

use Framework\Orm\Database;
use Framework\Orm\EntityRepository;
use Framework\Parser\YAMLParser;

class PostParagraphRepository extends EntityRepository
{
    private const CLASS_NAME = 'App\Entity\PostParagraph';

    public function __construct(YAMLParser $parser, Database $db)
    {
        parent::__construct($parser, $db, self::CLASS_NAME);
    }
}
