<?php

namespace App\Repository;

use Framework\Orm\Database;
use Framework\Orm\EntityRepository;
use Framework\Parser\YAMLParser;

class PostCommentRepository extends EntityRepository
{
    private const CLASS_NAME = 'App\Entity\PostComment';

    public function __construct(YAMLParser $parser, Database $db)
    {
        parent::__construct($parser, $db, self::CLASS_NAME);
    }

    public function findPostCommentaires($postId, $isAdmin)
    {
        $q = $this->createQueryBuilder('c')
            ->where('c.post_id = :post');

        if (!$isAdmin) {
            $q
                ->andWhere('c.visible = 1')
                ->andWhere('c.moderated = 1');
        }

        $q->setParameters(['post' => $postId]);

        $results = $q->getQuery()->getResult();
        return $this->hydrateMany($results);
    }
}
