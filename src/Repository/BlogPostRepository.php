<?php

namespace App\Repository;

use Framework\Orm\Database;
use Framework\Orm\EntityRepository;
use Framework\Parser\YAMLParser;

class BlogPostRepository extends EntityRepository
{
    private const CLASS_NAME = 'App\Entity\BlogPost';

    public function __construct(YAMLParser $parser, Database $db)
    {
        parent::__construct($parser, $db, self::CLASS_NAME);
    }

    /**
     * Return a list of posts for given options
     *
     * @param array $options
     * @return array
     * @throws \Framework\Orm\OrmNotFoundException
     */
    public function getList($options = [])
    {
        $q = $this->createQueryBuilder('p')
            ->orderBy('p.created_at', 'DESC');

        if (isset($options['category'])) {
            $q->join('post_category', 'c', 'p.category_id = c.id')
                ->where('c.id = :categoryId')
                ->setParameters(['categoryId' => $options['category']]);
        }

        $results = $q->getQuery()->getResult();

        return $this->hydrateMany($results);
    }
}
