<?php

namespace Framework\Container;

use PHPUnit\Framework\TestCase;
use Tests\App\Controller\AppController;
use Tests\App\Controller\Controller;
use Tests\App\Controller\TestController;

class ContainerTest extends TestCase
{
    public function testCanInstantiateClassWithConstructor()
    {
        $container = Container::getInstance();
        self::assertInstanceOf(Controller::class, $container->get(Controller::class));
    }

    public function testCanInstantiateClassWithoutConstructor()
    {
        $container = Container::getInstance();
        self::assertInstanceOf(AppController::class, $container->get(AppController::class));
    }

    public function testCanInstantiateConstructorWithDefaultValue()
    {
        $container = Container::getInstance();
        self::assertInstanceOf(TestController::class, $container->get(TestController::class));
    }

    public function testInstantiateNonExistingClass()
    {
        self::expectException(ContainerException::class);
        $container = Container::getInstance();
        $container->get('test');
    }

    public function testContainerGetRegisteredService()
    {
        $container = Container::getInstance();
        /** @var Controller $controller */
        $controller = $container->get(Controller::class);
        self::assertEquals($controller->getId(), $container->get(Controller::class)->getId());
    }
}