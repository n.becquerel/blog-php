<?php


namespace Tests\Framework\Http;


use Framework\Http\Stream;
use PHPUnit\Framework\TestCase;

class StreamTest extends TestCase
{
    protected $stream;

    protected function setUp(): void
    {
        $this->stream = new Stream('php://memory', 'w+');
    }

    public function testNewStreamInstance()
    {
        self::assertInstanceOf(Stream::class, $this->stream);
    }

    public function testToString()
    {
        self::assertEquals('', $this->stream);
    }
    public function testToStringWithData()
    {
        $stream = new Stream('php://memory', 'r+');
        $stream->write('test');
        self::assertEquals('test', $stream->__toString());
    }


    public function testIsWriteableShouldReturnFalse()
    {
        $stream = new Stream('php://memory', 'r');
        self::assertFalse($stream->isWritable());
    }
    public function testIsWriteableShouldReturnTrue()
    {
        $stream = new Stream('php://memory', 'r+');
        self::assertTrue($stream->isWritable());
    }

    public function testIsReadableShouldReturnTrue()
    {
        $stream = new Stream('php://memory', 'r');
        self::assertTrue($stream->isReadable());
    }

    public function testIsSeekable()
    {
        $stream = new Stream('php://memory', 'r');
        self::assertTrue($stream->isSeekable());
    }

    public function testTell()
    {
        $stream = new Stream('php://memory', 'r');
        self::assertEquals('0', $stream->tell());
    }
    public function testTellWithData()
    {
        $stream = new Stream('php://memory', 'r+');
        $stream->write('test');
        self::assertEquals(4, $stream->tell());
    }

    public function testRewind()
    {
        $stream = new Stream('php://memory', 'r+');
        $stream->write('test');
        $stream->rewind();
        self::assertEquals('0', $stream->tell());
    }

    public function testGetSize()
    {
        $stream = new Stream('php://memory', 'r+');
        self::assertEquals(0, $stream->getSize());
    }
    public function testGetSizeWithData()
    {
        $stream = new Stream('php://memory', 'r+');
        $stream->write('test');
        self::assertEquals(4, $stream->getSize());
    }

    public function isReadableShouldReturnTrue()
    {
        $stream = new Stream('php://memory', 'r');
        self::assertTrue($stream->isReadable());
    }
    public function isReadableShouldReturnFalse()
    {
        $stream = new Stream('php://memory', 'w');
        self::assertFalse($stream->isReadable());
    }

    public function testReadWithData()
    {
        $stream = new Stream('php://memory', 'r+');
        $stream->write('test');
        $stream->rewind();
        self::assertEquals('test', $stream->read($stream->getSize()));
    }

    public function testGetMetaData()
    {
        $stream = new Stream('php://memory', 'r+');
        self::assertIsArray($stream->getMetadata());
    }


    public function testEof()
    {
        $stream = new Stream('php://memory', 'w');
        $stream->write('test');
        $stream->read($stream->getSize());
        self::assertTrue($stream->eof());
    }


}