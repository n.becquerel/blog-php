<?php

namespace Tests\Framework\Http;

use Framework\Http\Uri;
use Framework\Http\UriFactory;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class UriFactoryTest extends TestCase
{
    private $uriFactory;

    protected function setUp(): void
    {
        $this->uriFactory = new UriFactory();
    }

    public function testNewUriEmptyString()
    {
        $uri = $this->uriFactory->createUri('/index.php?name=test#abc');
        $this->assertInstanceOf(Uri::class, $uri);
    }
}
