<?php

namespace Tests\App\Controller;

use Tests\App\Services\Service;

class Controller
{
    private $id;

    private $service;

    public function __construct(Service $service)
    {
        $this->id = uniqid();
        $this->service = $service;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }
}
