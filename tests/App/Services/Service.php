<?php

namespace Tests\App\Services;

class Service
{
    private $id;

    /**
     * Service constructor.
     * @param $id
     */
    public function __construct()
    {
        $this->id = uniqid();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }
}
