USE blog_php;
INSERT INTO `user` (`username`, `password`, `created_at`, `email`, `lastname`, `firstname`, `description`, `quote`,
                    `roles`)
    VALUES ('admin', '$2y$10$1m6VdqIFdDIeN9LRdC2qcec3cV6Fc./lA4u7P5BIKoPUZCvVmcexG', '2020-08-04', 'admin@blog-php.com',
        'Istrateur', 'Admin', 'Compte Administrateur', 'Admin', 'a:2:{i:0;s:10:"ROLE_ADMIN";i:1;s:9:"ROLE_USER";}'),
       ('user', '$2y$10$G.lwcGx/QMlcX6cWpdvSTODL2yd.B/n9aHs3mzvWa2QKJ2J8yNpvC', '2020-08-04', 'user@blog-php.com',
        'Isateur', 'Util', 'Compte utilisateur', 'User', 'a:1:{i:0;s:9:"ROLE_USER";}');

INSERT INTO `post_category` (`label`) VALUES ('PHP'), ('SYMFONY');

INSERT INTO `blog_post` (`short_description`, `created_at`, `main_title`, `content`, `updated_at`, `category_id`,
                         `user_id`)
    VALUES ('Ceci est mon premier post !!', '2020-08-05', 'Hello World !!!', null, null, 1, 1),
           ('Ceci est mon second post !!', '2020-08-05', 'Bonjour monde !!!', null, null, 2, 1),
           ('Ceci est mon premier post en utilisateur.', '2020-08-05', 'Bonjour je suis utilisateur', null, null, 1, 2),
           ('Ceci est mon second post en utilisateur', '2020-08-05', 'Bonjour, symfony est trop bien !', null, null, 2,
            2);

INSERT INTO `post_comment` (`user_id`, `created_at`, `body`, `visible`, `moderated`, `updated_at`, `post_id`)
    VALUES (1, '2020-08-05', 'Très bon article, mais manque un peu de contenu', 1, 1, null, 1),
           (1, '2020-08-05', 'Excellent ! ', 1, 1, null, 2),
           (1, '2020-08-05', 'Bonjour', 0, 0, null, 1),
           (2, '2020-08-05', 'Hello', 0, 0, null, 1),
           (2, '2020-08-05', 'Super ! ', 1, 1, null, 3);