CREATE DATABASE blog_php;
USE blog_php;

CREATE TABLE `user` (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(255),
    password TEXT,
    created_at DATETIME,
    email VARCHAR(255),
    lastname VARCHAR(255) DEFAULT NULL,
    firstname VARCHAR(255) DEFAULT NULL,
    description VARCHAR(255) DEFAULT NULL,
    quote VARCHAR(255) DEFAULT NULL,
    roles VARCHAR(255) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE post_category (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    label VARCHAR(255)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE blog_post (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    short_description VARCHAR(255),
    created_at DATETIME,
    main_title VARCHAR(255),
    content TEXT,
    updated_at DATETIME,
    category_id INT NOT NULL,
    user_id INT NOT NULL,
    FOREIGN KEY (user_id)
        REFERENCES `user` (id),
    FOREIGN KEY (category_id)
        REFERENCES post_category (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE post_comment (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    user_id INT NOT NULL,
    created_at DATETIME,
    body TEXT,
    visible TINYINT(1) DEFAULT 0,
    moderated TINYINT(1) DEFAULT 0,
    updated_at DATETIME,
    post_id INT NOT NULL,
    FOREIGN KEY (user_id)
        REFERENCES `user` (id),
    FOREIGN KEY (post_id)
        REFERENCES blog_post (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
